<?php
/* @var $this EntryController */
/* @var $model Entry */

$this->breadcrumbs=array(
	'Entries'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Entry', 'url'=>array('index')),
	array('label'=>'Create Entry', 'url'=>array('create')),
	array('label'=>'Update Entry', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Entry', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Entry', 'url'=>array('admin')),
);
?>

<h1>View Entry #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'alias',
		'folder',
		'title',
		'text',
		'author',
		'rating',
		'type',
		'published',
		'privacy',
		'privacy_list',
		'comments',
		'place',
		'date_created',
		'date_updated',
		'd_tags',
		'd_scale',
		'p_destination',
		'p_event_date',
		'p_date_publicated',
		'r_subject',
		'r_about',
		'r_evaluation',
		'r_ratio',
		'r_location',
		'r_purity',
		'r_comfort',
		'r_services',
		'r_personnel',
		'r_safety',
		'r_economy',
		'r_features',
		'r_appearance',
		'r_kitchen',
		'r_assortment',
		'r_situation',
		'r_service',
	),
)); ?>
