<?php
/* @var $this EntryController */
/* @var $model Entry */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#entry-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление записями</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'entry-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'alias',
		'folder',
		'title',
		'text',
		array(
			'name'=>'author',
		),

		/*
		'rating',
		'type',
		'published',
		'privacy',
		'privacy_list',
		'comments',
		'place',
		'date_created',
		'date_updated',
		'd_tags',
		'd_scale',
		'p_destination',
		'p_event_date',
		'p_date_publicated',
		'r_subject',
		'r_about',
		'r_evaluation',
		'r_ratio',
		'r_location',
		'r_purity',
		'r_comfort',
		'r_services',
		'r_personnel',
		'r_safety',
		'r_economy',
		'r_features',
		'r_appearance',
		'r_kitchen',
		'r_assortment',
		'r_situation',
		'r_service',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
