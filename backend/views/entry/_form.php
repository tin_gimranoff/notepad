<?php
/* @var $this EntryController */
/* @var $model Entry */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'entry-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'alias'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'folder'); ?>
		<?php echo $form->textField($model,'folder',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'folder'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'author'); ?>
		<?php echo $form->textField($model,'author',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'author'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rating'); ?>
		<?php echo $form->textField($model,'rating'); ?>
		<?php echo $form->error($model,'rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'published'); ?>
		<?php echo $form->textField($model,'published'); ?>
		<?php echo $form->error($model,'published'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy'); ?>
		<?php echo $form->textField($model,'privacy'); ?>
		<?php echo $form->error($model,'privacy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy_list'); ?>
		<?php echo $form->textField($model,'privacy_list',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'privacy_list'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textField($model,'comments'); ?>
		<?php echo $form->error($model,'comments'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'place'); ?>
		<?php echo $form->textField($model,'place'); ?>
		<?php echo $form->error($model,'place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'date_created'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_updated'); ?>
		<?php echo $form->textField($model,'date_updated',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'date_updated'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'d_tags'); ?>
		<?php echo $form->textArea($model,'d_tags',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'d_tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'d_scale'); ?>
		<?php echo $form->textField($model,'d_scale'); ?>
		<?php echo $form->error($model,'d_scale'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_destination'); ?>
		<?php echo $form->textField($model,'p_destination',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'p_destination'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_event_date'); ?>
		<?php echo $form->textField($model,'p_event_date',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'p_event_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'p_date_publicated'); ?>
		<?php echo $form->textField($model,'p_date_publicated',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'p_date_publicated'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_subject'); ?>
		<?php echo $form->textField($model,'r_subject',array('size'=>2,'maxlength'=>2)); ?>
		<?php echo $form->error($model,'r_subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_about'); ?>
		<?php echo $form->textField($model,'r_about',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'r_about'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_evaluation'); ?>
		<?php echo $form->textField($model,'r_evaluation'); ?>
		<?php echo $form->error($model,'r_evaluation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_ratio'); ?>
		<?php echo $form->textField($model,'r_ratio'); ?>
		<?php echo $form->error($model,'r_ratio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_location'); ?>
		<?php echo $form->textField($model,'r_location'); ?>
		<?php echo $form->error($model,'r_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_purity'); ?>
		<?php echo $form->textField($model,'r_purity'); ?>
		<?php echo $form->error($model,'r_purity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_comfort'); ?>
		<?php echo $form->textField($model,'r_comfort'); ?>
		<?php echo $form->error($model,'r_comfort'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_services'); ?>
		<?php echo $form->textField($model,'r_services'); ?>
		<?php echo $form->error($model,'r_services'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_personnel'); ?>
		<?php echo $form->textField($model,'r_personnel'); ?>
		<?php echo $form->error($model,'r_personnel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_safety'); ?>
		<?php echo $form->textField($model,'r_safety'); ?>
		<?php echo $form->error($model,'r_safety'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_economy'); ?>
		<?php echo $form->textField($model,'r_economy'); ?>
		<?php echo $form->error($model,'r_economy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_features'); ?>
		<?php echo $form->textField($model,'r_features'); ?>
		<?php echo $form->error($model,'r_features'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_appearance'); ?>
		<?php echo $form->textField($model,'r_appearance'); ?>
		<?php echo $form->error($model,'r_appearance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_kitchen'); ?>
		<?php echo $form->textField($model,'r_kitchen'); ?>
		<?php echo $form->error($model,'r_kitchen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_assortment'); ?>
		<?php echo $form->textField($model,'r_assortment'); ?>
		<?php echo $form->error($model,'r_assortment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_situation'); ?>
		<?php echo $form->textField($model,'r_situation'); ?>
		<?php echo $form->error($model,'r_situation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'r_service'); ?>
		<?php echo $form->textField($model,'r_service'); ?>
		<?php echo $form->error($model,'r_service'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->