<?php
/* @var $this EntryController */
/* @var $data Entry */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
	<?php echo CHtml::encode($data->alias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('folder')); ?>:</b>
	<?php echo CHtml::encode($data->folder); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('author')); ?>:</b>
	<?php echo CHtml::encode($data->author); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('published')); ?>:</b>
	<?php echo CHtml::encode($data->published); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('privacy')); ?>:</b>
	<?php echo CHtml::encode($data->privacy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('privacy_list')); ?>:</b>
	<?php echo CHtml::encode($data->privacy_list); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comments')); ?>:</b>
	<?php echo CHtml::encode($data->comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place')); ?>:</b>
	<?php echo CHtml::encode($data->place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_updated')); ?>:</b>
	<?php echo CHtml::encode($data->date_updated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_tags')); ?>:</b>
	<?php echo CHtml::encode($data->d_tags); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_scale')); ?>:</b>
	<?php echo CHtml::encode($data->d_scale); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_destination')); ?>:</b>
	<?php echo CHtml::encode($data->p_destination); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_event_date')); ?>:</b>
	<?php echo CHtml::encode($data->p_event_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('p_date_publicated')); ?>:</b>
	<?php echo CHtml::encode($data->p_date_publicated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_subject')); ?>:</b>
	<?php echo CHtml::encode($data->r_subject); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_about')); ?>:</b>
	<?php echo CHtml::encode($data->r_about); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_evaluation')); ?>:</b>
	<?php echo CHtml::encode($data->r_evaluation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_ratio')); ?>:</b>
	<?php echo CHtml::encode($data->r_ratio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_location')); ?>:</b>
	<?php echo CHtml::encode($data->r_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_purity')); ?>:</b>
	<?php echo CHtml::encode($data->r_purity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_comfort')); ?>:</b>
	<?php echo CHtml::encode($data->r_comfort); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_services')); ?>:</b>
	<?php echo CHtml::encode($data->r_services); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_personnel')); ?>:</b>
	<?php echo CHtml::encode($data->r_personnel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_safety')); ?>:</b>
	<?php echo CHtml::encode($data->r_safety); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_economy')); ?>:</b>
	<?php echo CHtml::encode($data->r_economy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_features')); ?>:</b>
	<?php echo CHtml::encode($data->r_features); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_appearance')); ?>:</b>
	<?php echo CHtml::encode($data->r_appearance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_kitchen')); ?>:</b>
	<?php echo CHtml::encode($data->r_kitchen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_assortment')); ?>:</b>
	<?php echo CHtml::encode($data->r_assortment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_situation')); ?>:</b>
	<?php echo CHtml::encode($data->r_situation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('r_service')); ?>:</b>
	<?php echo CHtml::encode($data->r_service); ?>
	<br />

	*/ ?>

</div>