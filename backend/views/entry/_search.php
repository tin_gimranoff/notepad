<?php
/* @var $this EntryController */
/* @var $model Entry */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alias'); ?>
		<?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'folder'); ?>
		<?php echo $form->textField($model,'folder',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'author'); ?>
		<?php echo $form->textField($model,'author',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rating'); ?>
		<?php echo $form->textField($model,'rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'published'); ?>
		<?php echo $form->textField($model,'published'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'privacy'); ?>
		<?php echo $form->textField($model,'privacy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'privacy_list'); ?>
		<?php echo $form->textField($model,'privacy_list',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'comments'); ?>
		<?php echo $form->textField($model,'comments'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'place'); ?>
		<?php echo $form->textField($model,'place'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_created'); ?>
		<?php echo $form->textField($model,'date_created',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_updated'); ?>
		<?php echo $form->textField($model,'date_updated',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'d_tags'); ?>
		<?php echo $form->textArea($model,'d_tags',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'d_scale'); ?>
		<?php echo $form->textField($model,'d_scale'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'p_destination'); ?>
		<?php echo $form->textField($model,'p_destination',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'p_event_date'); ?>
		<?php echo $form->textField($model,'p_event_date',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'p_date_publicated'); ?>
		<?php echo $form->textField($model,'p_date_publicated',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_subject'); ?>
		<?php echo $form->textField($model,'r_subject',array('size'=>2,'maxlength'=>2)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_about'); ?>
		<?php echo $form->textField($model,'r_about',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_evaluation'); ?>
		<?php echo $form->textField($model,'r_evaluation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_ratio'); ?>
		<?php echo $form->textField($model,'r_ratio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_location'); ?>
		<?php echo $form->textField($model,'r_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_purity'); ?>
		<?php echo $form->textField($model,'r_purity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_comfort'); ?>
		<?php echo $form->textField($model,'r_comfort'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_services'); ?>
		<?php echo $form->textField($model,'r_services'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_personnel'); ?>
		<?php echo $form->textField($model,'r_personnel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_safety'); ?>
		<?php echo $form->textField($model,'r_safety'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_economy'); ?>
		<?php echo $form->textField($model,'r_economy'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_features'); ?>
		<?php echo $form->textField($model,'r_features'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_appearance'); ?>
		<?php echo $form->textField($model,'r_appearance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_kitchen'); ?>
		<?php echo $form->textField($model,'r_kitchen'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_assortment'); ?>
		<?php echo $form->textField($model,'r_assortment'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_situation'); ?>
		<?php echo $form->textField($model,'r_situation'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'r_service'); ?>
		<?php echo $form->textField($model,'r_service'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->