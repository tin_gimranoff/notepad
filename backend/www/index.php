<?php

header('Content-Type: text/html; charset=utf-8');

defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

if (YII_DEBUG) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}

define('ROOTPATH', dirname(dirname(dirname(__FILE__))));
define('BACKPATH', ROOTPATH . DIRECTORY_SEPARATOR . 'backend' . DIRECTORY_SEPARATOR);
define('COMMONPATH', ROOTPATH . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR);

$yii = COMMONPATH . 'lib/framework/yii.php';
$config = BACKPATH . 'config/main.php';

require_once($yii);
$app = Yii::createWebApplication($config);
require_once(COMMONPATH . 'global.php');
$app->run();