<?php

/**
 * Class m130531_162608_create_entry_table
 *
 * Create entry table
 *
 * @author andymitrich
 * @version 1.0
 */
class m130531_162608_create_entry_table extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			'entry',
			array(
				// Entry parameters
				'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
				'alias' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
				'folder' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
				'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
				'text' => 'TEXT CHARACTER SET utf8 NOT NULL',
				'author' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
				'rating' => 'INT(5) SIGNED NOT NULL DEFAULT 0',
				'type' => 'TINYINT(2) UNSIGNED NOT NULL DEFAULT 0',
				'published' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
				'privacy' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
				'privacy_list' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
				'comments' => 'INT(5) SIGNED NOT NULL DEFAULT 0',
				'place' => 'INT(11) SIGNED NOT NULL DEFAULT 0',
				'date_created' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
				'date_updated' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
				// Dream additional parameters
				'd_tags' => 'TEXT CHARACTER SET utf8 NOT NULL DEFAULT ""',
				'd_scale' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
				// Prediction additional parameters
				'p_destination' => 'CHAR(255) CHARACTER SET utf8 DEFAULT ""',
				'p_event_date' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
				'p_date_publicated' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
				// Review additional parameters
				'r_subject' => 'INT(2) UNSIGNED NOT NULL DEFAULT 0',
				'r_about' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
				'r_evaluation' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
				'r_ratio' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_location' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_purity' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_comfort' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_services' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_personnel' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_safety' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_economy' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_features' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_appearance' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_kitchen' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_assortment' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_situation' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'r_service' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',

			),
			'ENGINE InnoDB CHARACTER SET utf8'
		);
	}

	public function down()
	{
		$this->dropTable('entry');
	}
}