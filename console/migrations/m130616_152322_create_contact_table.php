<?php

/**
 * Class m130616_152322_create_contact_table
 *
 * Create table for contacts
 *
 * @author andymitrich
 * @version 1.0
 */
class m130616_152322_create_contact_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('contact', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'owner_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'user_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'email'=>'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'group_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_created' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_updated' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('contact');
	}
}