<?php

/**
 * Class m130707_074850_create_message_table
 *
 * Create table for store user messages
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130707_074850_create_message_table extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            'message',
            array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'sender_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'recipient_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'text' => 'TEXT CHARACTER SET utf8 NOT NULL',
                'status' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
                'date_created' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_updated' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0'
            ),
            'ENGINE InnoDB CHARACTER SET utf8'
        );
    }

    public function down()
    {
        $this->dropTable('message');
    }
}