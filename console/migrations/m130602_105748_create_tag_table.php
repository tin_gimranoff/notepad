<?php

/**
 * Class m130602_105748_create_tag_table
 *
 * Create table for tags
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130602_105748_create_tag_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('tag', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'name' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT "" UNIQUE KEY',
                'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('tag');
	}
}