<?php

/**
 * Class m130602_105809_create_tag_reference
 *
 * Create table for store assignment between objects and tags
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130602_105809_create_tag_reference extends CDbMigration
{
	public function up()
	{
        $this->createTable('tag_reference', array(
                'object_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'tag_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'type' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'date_created' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                "UNIQUE KEY `name` (`object_id`,`tag_id`,`type`)"
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('tag_reference');
	}
}