<?php

/**
 * Class m130602_121514_create_author_table
 *
 * Create table for authors
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130602_121514_create_author_table extends CDbMigration
{
	public function up()
	{
        // TODO Index for user_id column
        $this->createTable('author', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'user_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'email' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'firstname' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'lastname' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""'
            ), 'ENGINE InnoDB CHARACTER SET utf8');

        $this->insert('author', array('user_id'=>1));
        $this->insert('author', array('user_id'=>2));
	}

	public function down()
	{
        $this->dropTable('author');
	}
}