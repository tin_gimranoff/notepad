<?php

/**
 * Class m130601_100929_create_user_table
 *
 * Create table for users
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130601_100929_create_user_table extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            'user',
            array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'alias' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'email' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'password' => 'CHAR(32) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'status' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'login' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'alter_email' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'alter_email_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'name' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'surname' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'surname_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'patronymic' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'patronymic_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'skype' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'skype_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'icq' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'icq_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'phone1' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'phone1_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'phone2' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'phone2_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'birthday' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'birthday_v' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
				'space_usage'=> 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_created' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_updated' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'last_login' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            ),
            'ENGINE InnoDB CHARACTER SET utf8'
        );

        $this->insert('user', array('email' => 'admin@admin.adm', 'password' => md5('admin@admin.adm' . 'admin')));
        $this->insert('user', array('email' => 'test@test.tst', 'password' => md5('test@test.tst' . 'test')));
    }

    public function down()
    {
        $this->dropTable('user');
    }
}