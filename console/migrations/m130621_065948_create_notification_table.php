<?php

/**
 * Class m130621_065948_create_notification_table
 *
 * Create table for notifications
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130621_065948_create_notification_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('notification', array(
                'id'=>'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'user_id'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'module_key'=>'TINYINT(2) UNSIGNED NOT NULL DEFAULT 0',
                'title'=>'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'text'=>'TEXT CHARACTER SET utf8 NOT NULL',
                'status' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'email'=>'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'date_created'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_updated'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_sended'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'INDEX (user_id, module_key)',
                'INDEX (user_id)',
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('notification');
	}
}