<?php

/**
 * Class m130629_095418_create_counter_table
 *
 * Store counters for users
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130629_095418_create_counter_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('counter', array(
                'user_id' => 'INT(11) UNSIGNED NOT NULL PRIMARY KEY',
                'entry' => 'INT(5) UNSIGNED NOT NULL DEFAULT 0',
                'comment' => 'INT(5) UNSIGNED NOT NULL DEFAULT 0',
                'service'=>'INT(5) UNSIGNED NOT NULL DEFAULT 0',
                'date_updated'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('counter');
	}
}