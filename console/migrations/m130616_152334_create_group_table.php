<?php

/**
 * Class m130616_152334_create_group_table
 *
 * Create table for contact groups
 *
 * @author andymitrich
 * @version 1.0
 */
class m130616_152334_create_group_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('group', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'owner_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'contact_count' => 'INT(5) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('group');
	}
}