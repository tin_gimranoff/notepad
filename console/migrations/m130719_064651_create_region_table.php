<?php

/**
 * Class m130719_064651_create_region_table
 *
 * Create table for storing regions
 */
class m130719_064651_create_region_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('region', array(
				'id'=>'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
				'name'=>'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
				'title'=>'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""'
			), 'ENGINE MyISAM CHARACTER SET utf8');
	}

	public function down()
	{
		$this->dropTable('region');
	}
}