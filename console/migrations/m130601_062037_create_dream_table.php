<?php

/**
 * Class m130601_062037_create_dream_table
 *
 * Create additional table for type of entry "Dream"
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130601_062037_create_dream_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('dream', array(
                'entry_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0 PRIMARY KEY',
                'tags' => 'TEXT CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'scale' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('dream');
	}
}