<?php

/**
 * Class m130531_162619_create_comment_table
 *
 * Create table for comments
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130531_162619_create_comment_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('comment', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'entry_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'hash' => 'CHAR(32) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'parent' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'text' => 'TEXT CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'rating' => 'INT(5) SIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('comment');
	}
}