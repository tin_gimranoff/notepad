<?php

/**
 * Class m130617_175631_create_subscribe_table
 *
 * Create table for subscribers
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130617_175631_create_subscribe_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('subscribe', array(
			'user_id'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
			'module_key'=>'TINYINT(2) UNSIGNED NOT NULL DEFAULT 0',
			'object_id'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'date_viewed'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
            'PRIMARY KEY (user_id, module_key, object_id)',
            'INDEX (user_id)',
            'INDEX (module_key, object_id)'
			), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
		$this->dropTable('subscribe');
	}
}
