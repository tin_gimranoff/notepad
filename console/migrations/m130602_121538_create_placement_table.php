<?php

/**
 * Class m130602_121538_create_placement_table
 *
 * Create table for places
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130602_121538_create_placement_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('placement', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'zip' => 'CHAR(8) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'country' => 'INT(3) UNSIGNED NOT NULL DEFAULT 0',
                'region' => 'INT(5) UNSIGNED NOT NULL DEFAULT 0',
                'city' => 'INT(5) UNSIGNED NOT NULL DEFAULT 0',
                'address' => 'CHAR(50) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'coordx' => 'CHAR(10) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'coordy' => 'CHAR(10) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'date_created' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('placement');
	}
}