<?php

/**
 * Class m130617_175631_create_follower_table
 *
 * Create table for followers
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130617_175631_create_follower_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('follower', array(
			'user_id'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
			'object_id'=>'INT(11) UNSIGNED NOT NULL DEFAULT 0',
			'object_type'=>'TINYINT(2) UNSIGNED NOT NULL DEFAULT 0'
			), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
		$this->dropTable('follower');
	}
}
