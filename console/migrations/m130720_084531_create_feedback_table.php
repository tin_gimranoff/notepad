<?php

/**
 * Class m130720_084531_create_feedback_table
 *
 * Create feedback table
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130720_084531_create_feedback_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('feedback',array(
                'id'=>'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'username'=>'CHAR(255) CHARACTER SET utf8 DEFAULT ""',
                'usermail'=>'CHAR(255) CHARACTER SET utf8 DEFAULT ""',
                'subject'=>'CHAR(255) CHARACTER SET utf8 DEFAULT ""',
                'text'=>'TEXT CHARACTER SET utf8 DEFAULT ""',
                'date_created'=>'INT(11) UNSIGNED NOT NULL'
            ),'ENGINE MyISAM CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('feedback');
	}
}