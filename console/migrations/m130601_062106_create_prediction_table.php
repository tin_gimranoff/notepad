<?php

/**
 * Class m130601_062106_create_prediction_table
 *
 * Create additional table for type of entry "Prediction"
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130601_062106_create_prediction_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('prediction', array(
                'entry_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0 PRIMARY KEY',
                'destination' => 'CHAR(255) CHARACTER SET utf8 DEFAULT ""',
                'event_date' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'date_publicated' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('prediction');
	}
}