<?php

/**
 * Class m130601_062141_create_review_table
 *
 * Create additional table for type of entry "Review"
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130601_062141_create_review_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('review', array(
                'entry_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0 PRIMARY KEY',
				'subject' => 'INT(2) UNSIGNED NOT NULL DEFAULT 0',
				'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'evaluation' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
				'ratio' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'location' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'purity' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'comfort' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'services' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'personnel' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'safety' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'economy' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'features' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'appearance' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'kitchen' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'assortment' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'situation' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
				'service' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 2',
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
		$this->dropTable('review');
	}
}