<?php

/**
 * Class m130608_060733_create_folder_table
 *
 * Create table for folders
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class m130608_060733_create_folder_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('folder', array(
                'id' => 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
                'parent_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'user_id' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0',
                'name' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'title' => 'CHAR(255) CHARACTER SET utf8 NOT NULL DEFAULT ""',
                'entry_count' => 'INT(5) UNSIGNED NOT NULL DEFAULT 0',
                'active' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 1',
                'privacy' => 'TINYINT(1) UNSIGNED NOT NULL DEFAULT 0',
                'order' => 'INT(11) UNSIGNED NOT NULL DEFAULT 0'
            ), 'ENGINE InnoDB CHARACTER SET utf8');
	}

	public function down()
	{
        $this->dropTable('folder');
    }
}