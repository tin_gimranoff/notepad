<?php

define('ROOTPATH', dirname(dirname(__FILE__)));
define('CONSOLEPATH', ROOTPATH . DIRECTORY_SEPARATOR . 'console' . DIRECTORY_SEPARATOR);
define('COMMONPATH', ROOTPATH . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR);

$yiic = COMMONPATH . 'lib/framework/yiic.php';
$config = CONSOLEPATH . 'config/console.php';

require_once($yiic);