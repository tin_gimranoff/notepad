<?php

/**
 * Class Author
 *
 * Класс для работы с сущностью "Автор"
 *
 * @property string $id
 * @property string $user_id
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Author extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Author the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'author';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('email', 'required'),
            array('user_id', 'length', 'max' => 11),
            array('email, firstname, lastname', 'length', 'max' => 255),
            array('id, user_id, email, firstname, lastname', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('firstname', $this->firstname, true);
        $criteria->compare('lastname', $this->lastname, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

	public static function getNameById($id) {

		if (!$id) return 'Аноним';

		/**
		 * @var $author Author
		 */
		$author = Author::model()->findByPk($id);

		if (!$author || !($author instanceof Author)) return 'Аноним';

		return $author->lastname;
	}
}