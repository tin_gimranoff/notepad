<?php

/**
 * Class Counter
 *
 * Class for processing counters of new entries, comments and etc.
 *
 * @property string $user_id
 * @property string $entry
 * @property string $comment
 * @property string $service
 * @property string $date_updated
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Counter extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Counter the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'counter';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id', 'required'),
            array('user_id, date_updated', 'length', 'max' => 11),
            array('entry, comment, service', 'length', 'max' => 5),
            array('user_id, entry, comment, service, date_updated', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_id' => 'User',
            'entry' => 'Entry',
            'comment' => 'Comment',
            'service' => 'Service',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('entry', $this->entry, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('service', $this->service, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}