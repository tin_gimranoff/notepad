<?php

/**
 * Class Entry
 *
 * Class for processing entries
 *
 * @property string $id
 * @property string $alias
 * @property string $folder
 * @property string $title
 * @property string $text
 * @property string $author
 * @property integer $rating
 * @property integer $type
 * @property integer $published
 * @property integer $privacy
 * @property string $privacy_list
 * @property integer $comments
 * @property integer $place
 * @property string $date_created
 * @property string $date_updated
 * @property string $d_tags
 * @property integer $d_scale
 * @property string $p_destination
 * @property string $p_event_date
 * @property string $p_date_publicated
 * @property string $r_subject
 * @property string $r_about
 * @property integer $r_evaluation
 * @property integer $r_ratio
 * @property integer $r_location
 * @property integer $r_purity
 * @property integer $r_comfort
 * @property integer $r_services
 * @property integer $r_personnel
 * @property integer $r_safety
 * @property integer $r_economy
 * @property integer $r_features
 * @property integer $r_appearance
 * @property integer $r_kitchen
 * @property integer $r_assortment
 * @property integer $r_situation
 * @property integer $r_service
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Entry extends ActiveRecord
{
	/**
	 * Entry types
	 */
	const ENTRY_TYPE_NORMAL = 1;
	const ENTRY_TYPE_DREAM = 2;
	const ENTRY_TYPE_PREDICTION = 3;
	const ENTRY_TYPE_REVIEW = 4;

	/**
	 * Dream Scale
	 */
	const DREAM_NEUTRAL = 1;
	const DREAM_BAD = 2;
	const DREAM_GOOD = 3;

	/**
	 * Review evaluation
	 */
	const REVIEW_EVALUATION_NEUTRAL = 1;
	const REVIEW_EVALUATION_GOOD = 2;
	const REVIEW_EVALUATION_BAD = 3;

	/**
	 * Review types
	 */
	const REVIEW_TYPE_WITHOUT_TYPE = 1;
	const REVIEW_TYPE_CAR = 2;
	const REVIEW_TYPE_RESTAURANT = 3;
	const REVIEW_TYPE_HOTEL = 4;

	/**
	 * Review scores
	 */
	const REVIEW_SCORE_TERRIBLE = 1;
	const REVIEW_SCORE_BAD = 2;
	const REVIEW_SCORE_NORMAL = 3;
	const REVIEW_SCORE_GOOD = 4;
	const REVIEW_SCORE_EXCELLENT = 5;

	public static $moduleKey = MODULE_KEY_ENTRY;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Entry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'entry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('text', 'required'),
			array('rating, type, published, privacy, comments, place, d_scale, r_evaluation, r_ratio, r_location, r_purity, r_comfort, r_services, r_personnel, r_safety, r_economy, r_features, r_appearance, r_kitchen, r_assortment, r_situation, r_service', 'numerical', 'integerOnly'=>true),
			array('alias, title, privacy_list, p_destination, r_about', 'length', 'max'=>255),
			array('folder, author, date_created, date_updated, p_event_date, p_date_publicated', 'length', 'max'=>11),
			array('r_subject', 'length', 'max'=>2),

			array('id, alias, folder, title, text, author, rating, type, published, privacy, privacy_list, comments, place, date_created, date_updated, d_tags, d_scale, p_destination, p_event_date, p_date_publicated, r_subject, r_about, r_evaluation, r_ratio, r_location, r_purity, r_comfort, r_services, r_personnel, r_safety, r_economy, r_features, r_appearance, r_kitchen, r_assortment, r_situation, r_service', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'alias' => 'Alias',
			'folder' => 'Folder',
			'title' => 'Title',
			'text' => 'Text',
			'author' => 'Author',
			'rating' => 'Rating',
			'type' => 'Type',
			'published' => 'Published',
			'privacy' => 'Privacy',
			'privacy_list' => 'Privacy List',
			'comments' => 'Comments',
			'place' => 'Place',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'd_tags' => 'D Tags',
			'd_scale' => 'D Scale',
			'p_destination' => 'P Destination',
			'p_event_date' => 'P Event Date',
			'p_date_publicated' => 'P Date Publicated',
			'r_subject' => 'R Subject',
			'r_about' => 'R About',
			'r_evaluation' => 'R Evaluation',
			'r_ratio' => 'R Ratio',
			'r_location' => 'R Location',
			'r_purity' => 'R Purity',
			'r_comfort' => 'R Comfort',
			'r_services' => 'R Services',
			'r_personnel' => 'R Personnel',
			'r_safety' => 'R Safety',
			'r_economy' => 'R Economy',
			'r_features' => 'R Features',
			'r_appearance' => 'R Appearance',
			'r_kitchen' => 'R Kitchen',
			'r_assortment' => 'R Assortment',
			'r_situation' => 'R Situation',
			'r_service' => 'R Service',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('folder',$this->folder,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('type',$this->type);
		$criteria->compare('published',$this->published);
		$criteria->compare('privacy',$this->privacy);
		$criteria->compare('privacy_list',$this->privacy_list,true);
		$criteria->compare('comments',$this->comments);
		$criteria->compare('place',$this->place);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('d_tags',$this->d_tags,true);
		$criteria->compare('d_scale',$this->d_scale);
		$criteria->compare('p_destination',$this->p_destination,true);
		$criteria->compare('p_event_date',$this->p_event_date,true);
		$criteria->compare('p_date_publicated',$this->p_date_publicated,true);
		$criteria->compare('r_subject',$this->r_subject,true);
		$criteria->compare('r_about',$this->r_about,true);
		$criteria->compare('r_evaluation',$this->r_evaluation);
		$criteria->compare('r_ratio',$this->r_ratio);
		$criteria->compare('r_location',$this->r_location);
		$criteria->compare('r_purity',$this->r_purity);
		$criteria->compare('r_comfort',$this->r_comfort);
		$criteria->compare('r_services',$this->r_services);
		$criteria->compare('r_personnel',$this->r_personnel);
		$criteria->compare('r_safety',$this->r_safety);
		$criteria->compare('r_economy',$this->r_economy);
		$criteria->compare('r_features',$this->r_features);
		$criteria->compare('r_appearance',$this->r_appearance);
		$criteria->compare('r_kitchen',$this->r_kitchen);
		$criteria->compare('r_assortment',$this->r_assortment);
		$criteria->compare('r_situation',$this->r_situation);
		$criteria->compare('r_service',$this->r_service);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Get list of types of entries
	 * @param null $id
	 * @return array
	 */
	public static function getTypeList($id = null)
	{

		$list = array(
			self::ENTRY_TYPE_NORMAL => 'Запись',
			self::ENTRY_TYPE_DREAM => 'Сон',
			self::ENTRY_TYPE_PREDICTION => 'Предсказание',
			self::ENTRY_TYPE_REVIEW => 'Отзыв'
		);

		return ($id) ? $list[$id] : $list;
	}

	/**
	 * Getting dream characteristics
	 * @param null $id
	 * @return array
	 */
	public static function getScaleList($id = null)
	{

		$list = array(
			self::DREAM_GOOD => 'Хороший сон',
			self::DREAM_BAD => 'Плохой сон',
			self::DREAM_NEUTRAL => 'Нейтральный сон'
		);

		return ($id)? $list[$id]: $list;
	}

	/**
	 * Get list of available privacy values
	 * @return array
	 */
	public static function getPrivacyList()
	{

		$list = array(
			'nothing' => 'Никому',
			'me' => 'Мне',
			'all' => 'Всем',
			'alternates' => 'Для выбранных пользователей',
		);

		return $list;
	}

	/**
	 * @param null $id
	 * @return array
	 */
	public static function getEvaluationList($id = null) {

		$list = array(
			self::REVIEW_EVALUATION_NEUTRAL => 'Нейтральный отзыв',
			self::REVIEW_EVALUATION_GOOD => 'Хороший отзыв',
			self::REVIEW_EVALUATION_BAD => 'Отрицательный отзыв'
		);

		return ($id)? $list[$id]: $list;
	}

	/**
	 * @param null $id
	 * @return array
	 */
	public static function getReviewTypesList($id = null) {

		$list = array(
			self::REVIEW_TYPE_WITHOUT_TYPE => 'Прочее',
			self::REVIEW_TYPE_CAR => 'Автомобиль',
			self::REVIEW_TYPE_HOTEL => 'Отель',
			self::REVIEW_TYPE_RESTAURANT => 'Ресторан'
		);

		return ($id)? $list[$id]: $list;
	}

	/**
	 * @param null $id
	 * @return array
	 */
	public static function getScoreList($id = null) {

		$list = array(
			self::REVIEW_SCORE_TERRIBLE => 'Ужасно',
			self::REVIEW_SCORE_BAD => 'Плохо',
			self::REVIEW_SCORE_NORMAL => 'Нормально',
			self::REVIEW_SCORE_GOOD => 'Хорошо',
			self::REVIEW_SCORE_EXCELLENT => 'Отлично'
		);

		return ($id !== null)? $list[$id]: $list;
	}

	/**
	 * Trigger "beforeSave"
	 * @return bool
	 */
	public function beforeSave()
	{

		if ($this->isNewRecord) {
			$this->date_created = time();
		}

		$this->date_updated = time();

		return parent::beforeSave();
	}

	/**
	 * Get list of entries by folder id
	 * @param $folderId
	 * @return array|bool
	 */
	public static function getEntriesByFolder($folderId)
	{
		if (!$folderId) {
			return false;
		}

		$command = command();
		$command
			->select()
			->from(self::tableName())
			->where("folder=$folderId");

		$rows = $command->queryAll();

		return $rows;
	}

	/**
	 * Trigger "afterSave"
	 */
	public function afterSave()
	{
		if (Yii::app()->hasComponent('user') && Yii::app()->user->id) {
			// Get id of current authorized user
			$userId = Yii::app()->user->id;

		}

		// Select array of subscribers of current user
		$rows = command()
			->select('user_id')
			->from(Subscribe::staticTableName())
			->where("object_id=" . $this->id . " AND module_key=" . self::$moduleKey)
			->queryAll();

		$subscribers = array();
		foreach ($rows as $row) {
			$subscribers[] = $row['user_id'];
		}

		// Add notification
		Notification::add();

		// Update date viewed for this entry
		/*
		command()->update(
			Subscribe::staticTableName(),
			array(
				'date_viewed' => 0
			),
			array(
				'module_key' => MODULE_KEY_ENTRY,
				'object_id' => $this->id
			)
		);
		*/

		parent::afterSave();
	}

	public static function getCountAdditional($count) {

		$output = '';
		switch ($count) {
			case 0: break;
			case 1: return '(' . $count . ' запись' . ')'; break;
			case 2:
			case 3:
			case 4: return '(' . $count . ' записи' . ')'; break;
			default: return '(' . $count . ' записей' . ')'; break;
		}

		return $output;
	}
}