<?php

/**
 * Class ActiveRecord
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class ActiveRecord extends CActiveRecord {}