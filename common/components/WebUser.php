<?php

/**
 * Class WebUser
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class WebUser extends CWebUser
{
	public $spaceUsage = 0;
    // TODO Проверить использование URL
    public $registerUrl = array("/site/register");
    public $recoveryUrl = array("/site/recovery");
    public $loginUrl = array("/site/login");
    public $logoutUrl = array("/site/logout");
    public $returnLogoutUrl = array("/site/login");

    public $captcha = array('registration' => false);

    /**
     * @param string $string
     * @return string
     */
    public function encrypting($string = '') {
        return Helper::encrypting($string);
    }

    /**
     * @param bool $fromCookie
     * TODO ЗАчем этот метод
     */
    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);
        $this->updateSession();
    }

    public function updateSession() {
        return true;
        $user = Yii::app()->user->id;
        $userAttributes = CMap::mergeArray(array(
                'email'=>$user->email,
                'username'=>$user->username,
                'create_at'=>$user->create_at,
                'lastvisit_at'=>$user->lastvisit_at,
            ),$user->profile->getAttributes());
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }
    }
}