<?php

/**
 * Class Helper
 */
class Helper
{
    protected static $dictionary = array(
        "й" => "i",
        "ц" => "c",
        "у" => "u",
        "к" => "k",
        "е" => "e",
        "н" => "n",
        "г" => "g",
        "ш" => "sh",
        "щ" => "shch",
        "з" => "z",
        "х" => "h",
        "ъ" => "",
        "ф" => "f",
        "ы" => "y",
        "в" => "v",
        "а" => "a",
        "п" => "p",
        "р" => "r",
        "о" => "o",
        "л" => "l",
        "д" => "d",
        "ж" => "zh",
        "э" => "e",
        "ё" => "e",
        "я" => "ya",
        "ч" => "ch",
        "с" => "s",
        "м" => "m",
        "и" => "i",
        "т" => "t",
        "ь" => "",
        "б" => "b",
        "ю" => "yu",
        "Й" => "I",
        "Ц" => "C",
        "У" => "U",
        "К" => "K",
        "Е" => "E",
        "Н" => "N",
        "Г" => "G",
        "Ш" => "SH",
        "Щ" => "SHCH",
        "З" => "Z",
        "Х" => "X",
        "Ъ" => "",
        "Ф" => "F",
        "Ы" => "Y",
        "В" => "V",
        "А" => "A",
        "П" => "P",
        "Р" => "R",
        "О" => "O",
        "Л" => "L",
        "Д" => "D",
        "Ж" => "ZH",
        "Э" => "E",
        "Ё" => "E",
        "Я" => "YA",
        "Ч" => "CH",
        "С" => "S",
        "М" => "M",
        "И" => "I",
        "Т" => "T",
        "Ь" => "",
        "Б" => "B",
        "Ю" => "YU",
        "«" => "",
        "»" => "",
        " " => "-",
        "\"" => "",
        "\." => "",
        "–" => "-",
        "\," => "",
        "\(" => "",
        "\)" => "",
        "\?" => "",
        "\!" => "",
        "\:" => "",
        '#' => '',
        '№' => '',
        ' - ' => '-',
        '/' => '-',
        '  ' => '-',
    );

    public static function translit($text, $toLowerCase = true)
    {

        // Enforce the maximum component length
        $maxLength = 100;

        $text = implode(
            array_slice(
                explode('<br>', wordwrap(trim(strip_tags(html_entity_decode($text))), $maxLength, '<br>', false)),
                0,
                1
            )
        );
        //$text = substr(, 0, $maxlength);

        foreach (self::$dictionary as $from => $to) {
            $text = mb_eregi_replace($from, $to, $text);
        }

        // Optionally convert to lower case.
        if ($toLowerCase) {
            $text = strtolower($text);
        }

        return $text;
    }

    /**
     * @param string $place
     * @return bool
     */
    public static function doCaptcha($place = '')
    {
        if (!extension_loaded('gd')) {
            return false;
        }

        if (in_array($place, Yii::app()->user->captcha)) {
            return Yii::app()->user->captcha[$place];
        }

        return false;
    }

    /**
     * @param string $string
     * @return string
     */
    public static function encrypting($string = "")
    {
        return md5($string);
    }

}