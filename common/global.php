<?php

define('MODULE_KEY_USER', 1);
define('MODULE_KEY_ENTRY', 2);
define('MODULE_KEY_COMMENT', 3);

/**
 * Snippet for getting Application
 * @return CWebApplication
 */
function app() {
    return Yii::app();
}

/**
 * Snippet for getting DB connection
 * @return CDbConnection
 */
function db() {
    return Yii::app()->db;
}

/**
 * Snippet for getting ClientScript adapter
 * @return CClientScript
 */
function clientScript() {
    return Yii::app()->clientScript;
}

/**
 * Snippet for getting http request
 * @return CHttpRequest
 */
function request() {
    return Yii::app()->request;
}

/**
 * Snippet for getting DB command
 * @param string $query
 * @return CDbCommand
 */
function command($query = '') {
    return Yii::app()->db->createCommand($query);
}

/**
 * Snippet for creating url
 * @param $route
 * @param array $params
 * @param string $ampersand
 * @return string
 */
function url($route,$params=array(),$ampersand='&') {
    return Yii::app()->urlManager->createUrl($route,$params=array(),$ampersand='&');
}

/**
 * Snippet for getting user
 * @return RWebUser
 */
function user() {
    return Yii::app()->user;
}