<?php

Yii::setPathOfAlias('common', COMMONPATH);
Yii::setPathOfAlias('rights.RightsModule', COMMONPATH . 'modules/rights/RightsModule');
require_once('db.php');

$db_config = getDBconfig();

return array(
    'preload' => array('log'),
	'language' => 'ru',
    'import' => array(
        'common.components.*',
		'common.models.*',
        'common.extensions.*',
        'common.modules.rights.*',
        'common.modules.rights.components.*',
        'common.helpers.*',
        'common.extensions.yii-mail.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => '1',
            'ipFilters' => array('127.0.0.1','::1'),
        ),
        'rights' => array(
            'install' => false,
            'debug' => false,
        ),
    ),
    'components' => array(
        'db' => $db_config,
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => true,
        ),
        'authManager' => array(
            'class' => 'RDbAuthManager',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            /*'enabled'=>YII_DEBUG,
            'routes'=>array(
                array(
                    'class'=>'common.extensions.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'ipFilters'=>array('127.0.0.1','192.168.1.215'),
                ),
            ),*/
        ),
        'mailer' => array(
            'class' => 'common.extensions.mailer.EMailer',
            'pathViews' => 'common.views.email',
            'pathLayouts' => 'common.views.email.layouts'
        ),
    /*
        'mail' => array(
 			'class' => 'common.extensions.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => '',
                'username' => '',
                'password' => '',
                'port' => '',
                'encryption'=>'tls',
            ),


 			'viewPath' => 'application.views.mail',
 			'logging' => true,
 			'dryRun' => false
 		),
          */
    ),
);