<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

$common = require_once(COMMONPATH . 'config/common.php');
$params = require_once('params.php');

return CMap::mergeArray(
    $common,
    array(
        'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR.'..',
        'name' => 'Блокнот.рф',
        'preload' => array('log'),
		'import' => array(
			'application.components.*',
			'application.extensions.*',
			'application.models.*',
		),
        'modules' => array(
        ),
        'components' => array(
            // uncomment the following to enable URLs in path-format
            /*
            'urlManager'=>array(
                'urlFormat'=>'path',
                'rules'=>array(
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),
            */
            'errorHandler' => array(
                'errorAction' => 'site/error',
            ),

        ),
        'params' => $params
));