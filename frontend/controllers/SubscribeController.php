<?php

/**
 * Class SubscribeController
 * 
 * Controller for subscribe processes
 * 
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class SubscribeController extends FrontEndController {

    public function actionAdd() {

        if (request()->isAjaxRequest) {
            echo 'add';
        }
    }

    public function actionDelete() {

        if (request()->isAjaxRequest) {
            echo 'delete';
        }
    }
}