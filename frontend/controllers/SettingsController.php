<?php

/**
 * Class SettingsController
 *
 * Controller for user settings
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class SettingsController extends FrontEndController
{

    public function actionIndex()
    {
        if (app()->hasComponent('user') && user()->id) {

            $userId = user()->id;
            $userData = request()->getPost('User', array());

            /**
             * @var $model User
             */
            $model = $this->loadModel($userId);

            if (sizeof($userData)) {
                
                $model->login = isset($userData['login'])? $userData['login']: '';
                $model->name = isset($userData['name'])? $userData['name']: '';
                $model->surname = isset($userData['surname'])? $userData['surname']: '';
                $model->surname_v = isset($userData['surname_v'])? $userData['surname_v']: '';
                $model->patronymic = isset($userData['patronymic'])? $userData['patronymic']: '';
                $model->patronymic_v = isset($userData['patronymic_v'])? $userData['patronymic_v']: '';
                $model->skype = isset($userData['skype'])? $userData['skype']: '';
                $model->skype_v = isset($userData['skype_v'])? $userData['skype_v']: '';
                $model->icq = isset($userData['icq'])? $userData['icq']: '';
                $model->icq_v = isset($userData['icq_v'])? $userData['icq_v']: '';
                $model->phone1 = isset($userData['phone1'])? $userData['phone1']: '';
                $model->phone1_v = isset($userData['phone1_v'])? $userData['phone1_v']: '';
                $model->phone2 = isset($userData['phone2'])? $userData['phone2']: '';
                $model->phone2_v = isset($userData['phone2_v'])? $userData['phone2_v']: '';
                $model->birthday = isset($userData['birthday'])? $userData['birthday']: '';
                $model->birthday_v = isset($userData['birthday_v'])? $userData['birthday_v']: '';
                $model->alter_email = isset($userData['alter_email'])? $userData['alter_email']: '';
                $model->alter_email_v = isset($userData['alter_email_v'])? $userData['alter_email_v']: '';

                // If main email is changed
                if (isset($userData['email']) && $model->email != $userData['email']) {
                    // TODO Отправка письма с активацией
                    // Set ticket
                    // Send email
                }

                $model->save(false);
            }

            $this->render('index', array('model'=>$model));
        }
        else $this->redirect(array('site/login'));
    }

    public function actionChangePassword()
    {

        $this->render('change_password', array());
    }

    public function actionChangeAvatar()
    {
        $this->render('change_avatar', array(
            ));
    }

    public function actionSpaceUsage()
    {

        $this->render('space_usage', array());
    }

    public function actionBalance()
    {

        $this->render('balance', array());
    }

    public function actionRemoveAccount()
    {

        $this->render('remove_account', array());
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contact the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }
}