<?php

/**
 * Class PlaceController
 *
 * Controller for placements
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class PlaceController extends FrontEndController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Place;

        if (isset($_POST['Place'])) {
            $model->attributes = $_POST['Place'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Place'])) {
            $model->attributes = $_POST['Place'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $placement = new Place();

        $models = array();
        $models = Place::model()->findAll();

        $data = array();
        if (sizeof($models)) {

            /**
             * @var $model Place
             */
            foreach ($models as $model) {
                $data[] = array(
                    'x' => $model->coordx,
                    'y' => $model->coordy,
                    'content' => 'Москва',
                    'baloonContent' => 'Столица'
                );
            }

        }

        clientScript()->registerScript('places', 'var places = ' . CJavaScript::jsonEncode($data), CClientScript::POS_HEAD);

        $this->render(
            'index',
            array(
                'placement' => $placement
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Place the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Place::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Placement $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'placement-form') {
            echo ActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
