<?php

class NotificationController extends FrontEndController
{
	public function filters()
	{
		return array();
	}

    public function actionIndex()
    {
        if (app()->hasComponent('user') && user()->id) {

            $userId = user()->id;

            $list = Notification::model()->findAllByAttributes(array('user_id'=>$userId));

            $this->render('index', array(
                    'list' => $list
                ));
        }
        else throw new CHttpException(404, 'The requested page does not exist.');
    }
}