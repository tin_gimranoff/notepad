<?php

/**
 * Class EntryController
 *
 * Контроллер записей
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class EntryController extends FrontEndController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $entry = new Entry();

        $place = new Place();

        if (request()->getPost('Entry', null)) {

            // Getting info about author
            $authorData = request()->getPost('Author', array());
            if (sizeof($authorData)) {

                $email = (isset($authorData['email'])) ? $authorData['email'] : '';

                // Check email
                if ($email) {

                    // Check duplicate author by email
                    $author = Author::model()->findByAttributes(array('email' => $email));
                    if (!($author instanceof Author)) {
                        $author = new Author();

                        $author->attributes = $authorData;
                        $author->save();
                    }
                }
            }

            // Save base entry
            $entry->attributes = request()->getPost('Entry');
            $entry->author = (isset($author) && $author instanceof Author) ? $author->id : 0;
            $entry->save(false);

            // Save additional data of entry
//			switch ($entry->type) {
//
//				case Entry::ENTRY_TYPE_DREAM:
//
//					// Getting info for dream type
//					$dreamData = request()->getPost('Dream', array());
//
//					if (sizeof($dreamData)) {
//
//						$dream = new Dream();
//						$dream->attributes = $dreamData;
//						$dream->entry_id = $entry->id;
//						$dream->save();
//
//						// Getting and save tags
//						if ($dream->tags) {
//
//							$tags = explode(',', $dream->tags);
//
//							foreach ($tags as $tagTitle) {
//
//								$tagName = Tag::normalizeTag($tagTitle);
//
//								$tag = Tag::model()->findByAttributes(array('name' => $tagName));
//
//								if (!($tag instanceof Tag)) {
//
//									$tag = new Tag();
//									$tag->name = $tagName;
//									$tag->title = $tagTitle;
//									$tag->save();
//								}
//
//								TagReference::insertOrIgnoreTag($tag->id, $entry->id);
//							}
//						}
//					}
//
//					break;
//
//				case Entry::ENTRY_TYPE_PREDICTION:
//
//					// Getting info for prediction type
//					$predictionData = request()->getPost('Prediction', array());
//
//					if (sizeof($predictionData)) {
//
//						$prediction = new Prediction();
//						$prediction->attributes = $predictionData;
//						$prediction->entry_id = $entry->id;
//						$prediction->save();
//					}
//
//					break;
//
//				case Entry::ENTRY_TYPE_REVIEW:
//
//					// Getting info for review type
//					$reviewData = request()->getPost('Review', array());
//
//					if (sizeof($reviewData)) {
//
//						$review = new Review();
//						$review->attributes = $reviewData;
//						$review->entry_id = $entry->id;
//						$review->save();
//					}
//
//					break;
//
//				default:
//					break;
//			}

            if ($entry->folder) {
                $this->redirect(array('folder/index', 'id' => $entry->id));
            } else {
                if ($entry->id) {
                    $this->redirect(array('view', 'id' => $entry->id));
                } else {
                    $this->redirect(array('entry/index', 'id' => $entry->id));
                }
            }
        }

        clientScript()->registerScript(
            'entryTypes',
            'var entry_type_normal = ' . Entry::ENTRY_TYPE_NORMAL .
            ';var entry_type_dream = ' . Entry::ENTRY_TYPE_DREAM .
            ';var entry_type_prediction = ' . Entry::ENTRY_TYPE_PREDICTION .
            ';var entry_type_review = ' . Entry::ENTRY_TYPE_REVIEW,
            CClientScript::POS_HEAD
        );

        $author = new Author();

        $folder = request()->getParam('folder', 0);

        $this->render(
            'create',
            array(
                'entry' => $entry,
                'author' => $author,
                'place' => $place,
                'folder' => $folder
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Entry'])) {
            $model->attributes = $_POST['Entry'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
//        $message = new YiiMailMessage;
//        $message->setBody('Message content here with HTML', 'text/html');
//        $message->subject = 'My Subject';
//        $message->addTo(Yii::app()->params['adminEmail']);
//        $message->from = 'xxx@yyy.zzz';
//        Yii::app()->mail->send($message);
        //echo "<PRE>"; print_r(Yii::app()->mail); echo "</PRE>";


        $criteria = new CDbCriteria();
        $criteria->condition = 'published = 1';

        if (request()->getQuery('type', 0)) {
            $type = request()->getQuery('type', 0);
            if ($type)
                $criteria->addCondition('type=' . $type);
        }

        $dataProvider = new CActiveDataProvider('Entry', array('criteria' => $criteria));
        $this->render(
            'index',
            array(
                'dataProvider' => $dataProvider,

            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Entry the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Entry::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Entry $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'entry-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
