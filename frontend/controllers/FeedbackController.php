<?php

/**
 * Class FeedbackController
 *
 * Controller for display feedback form
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class FeedbackController extends FrontEndController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Feedback;

        if (isset($_POST['Feedback'])) {
            $model->attributes = $_POST['Feedback'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(array('create'));
    }
}
