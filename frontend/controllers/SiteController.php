<?php

class SiteController extends FrontEndController
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * Default action
     */
    public function actionIndex()
    {
        $login = new LoginForm();

        $this->render('index', array(
                'login' => $login
            ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    public function actionLogin()
    {
        if (Yii::app()->user->isGuest) {

            $model = new LoginForm();

            $loginData = request()->getPost('LoginForm');
            if (sizeof($loginData)) {
                $model->attributes = $loginData;

                if ($model->validate()) {
                    $this->redirect(array('site/index'));
                }
            }

            $this->render('/site/login', array('model' => $model));
        } else {
            $this->redirect(array('site/index'));
        }
    }

    public function actionRegister()
    {
        $model = new RegistrationForm();

        // ajax validator
        if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
        {
            echo ActiveForm::validate($model);
            Yii::app()->end();
        }

        if (Yii::app()->user->id) {
            $this->redirect(array('site/index'));
        } else {
            if (isset($_POST['RegistrationForm'])) {

                $model->attributes = $_POST['RegistrationForm'];

                if ($model->validate()) {

                    $model->password = Helper::encrypting($model->email . $model->password);

                    if ($model->save()) {
                        $this->redirect(array('site/index'));
                    }
                }
            }

            $this->render('/site/register', array('model' => $model));
        }
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
	
	public function actionActivate()
	{
		if(isset($_GET['u']) && isset($_GET['code']) && !empty($_GET['u']) && !empty($_GET['code']))
		{
			$user = User::model()->findByPk((int)$_GET['u']);
			if($user == null)
			{
				$this->redirect('/');
				return;
			} 
			if(md5($user->id.$user->email.$user->password) == $_GET['code'])
			{
				$user->status = 1;
				$user->last_login = time();
				$user->save();
	            $identity = new UserIdentity($user->email, $user->password);
				$identity->authenticate();
	            Yii::app()->user->login($identity);
				$this->redirect('/');
				return;
			} else {
				$this->redirect('/');
				return;
			}
		} else {
			$this->redirect('/');
			return;
		}
	}
    
    public function actionRestore()
    {
        if(isset($_POST['email']) && !empty($_POST['email']))
        {
            $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
            $max = 8;
            $size = strlen($chars)-1;
            $password = null;
            while($max--) 
                $password .= $chars[rand(0,$size)];
            $user = User::model()->findByAttributes(array('email' => $_POST['email']));
            if($user == null)
            {
               $this->redirect('/');
                return; 
            } else {
                $user->password = Helper::encrypting($user->email . $password);
                if($user->save())
                {
                        $email = Yii::app()->mailer;
                        $email->CharSet = 'utf-8';
                        $email->ContentType = 'text/html';
                        $email->From = Yii::app()->params['adminEmail'];
                        $email->FromName = 'Robot Notepad';
                        $email->ClearAddresses();
                        $email->AddAddress($model->email);
                        $email->Subject = 'Новый пароль';
                        $email->Body = "Новый пароль: ".$password;
                        $email->Send();  
                        
                }
            }   
        } else {
            $this->redirect('/');
            return;
        }
    }

    public function actionAgreement() {

        $this->render('_agreement');
    }
	
	public function actionSetAddress()
	{
        Yii::app()->session['city'] = '';
        Yii::app()->session['lat'] = '';
        Yii::app()->session['lng'] = '';
        Yii::app()->session['address'] = '';
        Yii::app()->session['region'] = '';
        
		if(isset($_GET['city']) && !empty($_GET['city']))
			Yii::app()->session['city'] = (string)$_GET['city'];
		if(isset($_GET['lat']) && !empty($_GET['lat']))
			Yii::app()->session['lat'] = (string)$_GET['lat'];
		if(isset($_GET['lng']) && !empty($_GET['lng']))
			Yii::app()->session['lng'] = (string)$_GET['lng'];
		if(isset($_GET['address']) && !empty($_GET['address']))
			Yii::app()->session['address'] = (string)$_GET['address'];
		if(isset($_GET['region']) && !empty($_GET['region']))
			Yii::app()->session['region'] = (string)$_GET['region'];
	}
}