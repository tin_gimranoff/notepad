<?php

/**
 * Class GroupController
 *
 * Controller for groups
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class GroupController extends FrontEndController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if (app()->hasComponent('user') && user()->id) {

            $model = new Group;
            $groupData = request()->getPost('Group', array());

            if (sizeof($groupData)) {

                $model->attributes = $_POST['Group'];
                $model->owner_id = user()->id;

                if ($model->save()) {

                    $this->redirect(array('index'));
                }
            }

            $this->render(
                'create',
                array(
                    'model' => $model,
                )
            );
        } else {
            throw new Exception(404, 'The requested page does not exist.');
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        if (app()->hasComponent('user') && user()->id) {

            $model = $this->loadModel($id);
            $groupData = request()->getPost('Group', array());

            if (sizeof($groupData)) {

                $model->attributes = $_POST['Group'];

                if ($model->save()) {

                    $this->redirect(array('index'));
                }
            }

            $this->render(
                'update',
                array(
                    'model' => $model,
                )
            );
        } else {
            throw new Exception(404, 'The requested page does not exist.');
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws Exception
     */
    public function actionDelete($id)
    {
        if (app()->hasComponent('user') && user()->id) {

            $this->loadModel($id)->delete();

            if (request()->getQuery('ajax')) {

                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
        } else {
            throw new Exception(404, 'The requested page does not exist.');
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        if (app()->hasComponent('user') && user()->id) {

            $contact = new Contact();
            $group = new Group();
            $ownerId = user()->id;
            $groups = Group::model()->findAllByAttributes(array('owner_id' => $ownerId));
            $this->render(
                'index',
                array(
                    'contact' => $contact,
                    'group' => $group,
                    'list' => $groups
                )
            );
        } else {
            $this->redirect(array('site/login'));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Group the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Group::model()->findByPk($id);

        if ($model === null) {

            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Group $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'group-form') {

            echo ActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionLoadContacts()
    {
        if (request()->isAjaxRequest) {
            $group = new Group();
            echo $this->render('_inner_extended_row', array('group'=>$group));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }
}
