<?php

/**
 * Class FolderController
 *
 * Controller of folders
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class FolderController extends FrontEndController
{
    /**
     * @throws CHttpException
     */
    public function actionIndex()
	{
		$model = new Folder();

		if (app()->hasComponent('user') && !user()->isGuest) {
			//$list = $model->getFolderList(user()->id);

            $list = Folder::model()->findAllByAttributes(array('user_id'=>user()->id));

			$this->render(
				'index',
				array(
					'folder' => $model,
					'folderList' => $list,
				)
			);
		} else {
			$this->redirect(array('site/login'));
		}
	}

	/**
	 * Create folder
	 */
	public function actionCreate()
	{
		$model = new Folder();
		$folderData = request()->getPost('Folder', array());

		if (sizeof($folderData)) {

			$model->unsetAttributes();
			$model->attributes = $folderData;
			$model->name = Helper::translit($model->title);
			if (app()->hasComponent('user') && !user()->isGuest) {
				$model->user_id = user()->id;
			}

			$model->save();
		}

		$this->redirect(array('folder/index'));
	}

	public function actionEntries()
	{
		if (request()->isAjaxRequest) {

			$result = array();
			$folderId = request()->getPost('folder', 0);

			if ($folderId) {
				$result = Entry::getEntriesByFolder($folderId);
			}

			echo CJSON::encode($result);
			app()->end();
		}
	}

	public function actionUpdate($id)
	{
		if (request()->isAjaxRequest) {

			$folder = $this->loadModel($id);

			$this->renderPartial('update', array(
					'model'=>$folder
				));
		}
		else {
			$folderData = request()->getPost('Folder');

			if (sizeof($folderData)) {
				$folder = $this->loadModel($id);
				$folder->setAttributes($folderData);
				$folder->save(false);
			}

			$this->redirect(array('folder/index'));
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Message the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Folder::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	public function actionDelete($id) {

		if (app()->hasComponent('user') && !user()->isGuest) {

			if (!$id) throw new CHttpException(404, 'The requested page does not exist.');

			/**
			 * @var $folder Folder
			 */
			$folder = $this->loadModel($id);

			if ($folder instanceof Folder) {
				$folder->delete();
			}

			$this->redirect(array('folder/index'));
		} else {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
	}
}