<?php

/**
 * Class ContactController
 *
 * Controller for processing contacts
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class ContactController extends FrontEndController
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @throws Exception
     */
    public function actionCreate()
    {
        if (app()->hasComponent('user') && user()->id) {
            $model = new Contact;
            $contactData = request()->getPost('Contact');

            if (sizeof($contactData)) {

                $model->attributes = $contactData;
                $model->owner_id = user()->id;

                if ($model->save()) {
                    $this->redirect(array('index'));
                }
            }

            $this->render(
                'create',
                array(
                    'model' => $model,
                )
            );
        } else {
            throw new Exception(404, 'The requested page does not exist.');
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @throws Exception
     */
    public function actionUpdate($id)
    {
        if (app()->hasComponent('user') && user()->id) {
        $model = $this->loadModel($id);

        if (isset($_POST['Contact'])) {
            $model->attributes = $_POST['Contact'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
        }else {
            throw new Exception(404, 'The requested page does not exist.');
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $contactList = array();

        // Check user authorization
        if (app()->hasComponent('user') && user()->id) {

            $ownerId = user()->id;

            $rows = Contact::model()->findAllByAttributes(array('owner_id'=>$ownerId));
            $model = new Contact();

            $this->render(
                'index',
                array(
                    'list' => $rows,
                    'model' => $model
                )
            );
        } else {
            $this->redirect(array('site/login'));
        }


    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contact the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Contact::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Contact $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contact-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionComplete()
    {

        $query = request()->getQuery('q', '');
        $result = array();

        if (!empty($query)) {

            $command = command()
                ->select('name, surname, email')
                ->from('user');

            $where = "
                name LIKE '%$query%' OR
                surname LIKE '%$query%' OR
                email LIKE '%$query%'
            ";
            $command->setWhere($where);

            $rows = $command->queryAll();

            if (sizeof($rows)) {
                foreach ($rows as $row) {

                    $current = array();
                    $current['value'] = $row['email'];
                    $current['full'] = "{$row['name']} {$row['surname']} <{$row['email']}>";
                    $result[] = $current;;
                }
            }
        }

        echo CJSON::encode($result);;
        Yii::app()->end();
    }
}
