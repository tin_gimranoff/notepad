<?php

/**
 * Class TagReference
 *
 * Класс для работы со ссылками типа объект=>тэг
 *
 * @property string $object_id
 * @property string $tag_id
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class TagReference extends ActiveRecord
{
    const RERERENCE_TYPE_ENTRY = 0;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TagReference the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tag_reference';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('object_id, tag_id', 'length', 'max' => 11),
            array('object_id, tag_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'object_id' => 'Object',
            'tag_id' => 'Tag',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('object_id', $this->object_id, true);
        $criteria->compare('tag_id', $this->tag_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function insertOrIgnoreTag($tagId, $objectId, $type = self::RERERENCE_TYPE_ENTRY, $datetime = '') {

        if (!$datetime) $datetime = time();

        // Build command for tag reference
        $sqlReference = "
            INSERT IGNORE INTO
                " . self:: tableName() . "
            SET
                `object_id` = :object_id,
                `tag_id` = :tag_id,
                `type` = :type,
                `date_created` = :currenttime;";

        $cmdReference = command($sqlReference);

        $cmdReference->bindValue('tag_id', $tagId);
        $cmdReference->bindValue('object_id', $objectId);
        $cmdReference->bindValue('type', $type);
        $cmdReference->bindValue('currenttime', $datetime);

        return $cmdReference->execute();
    }
}