<?php

/**
 * Class Review
 *
 * Model for processing type of entries "Review"
 *
 * @property string $entry_id
 * @property string $subject
 * @property string $title
 * @property integer $evaluation
 * @property integer $ratio
 * @property integer $location
 * @property integer $purity
 * @property integer $comfort
 * @property integer $services
 * @property integer $personnel
 * @property integer $safety
 * @property integer $economy
 * @property integer $features
 * @property integer $appearance
 * @property integer $kitchen
 * @property integer $assortment
 * @property integer $situation
 * @property integer $service
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Review extends ActiveRecord
{
    const REVIEW_EVALUATION_NEUTRAL = 1;
    const REVIEW_EVALUATION_GOOD = 2;
    const REVIEW_EVALUATION_BAD = 3;

    const REVIEW_TYPE_WITHOUT_TYPE = 1;
    const REVIEW_TYPE_CAR = 2;
    const REVIEW_TYPE_RESTAURANT = 3;
    const REVIEW_TYPE_HOTEL = 4;

    const REVIEW_SCORE_TERRIBLE = 1;
    const REVIEW_SCORE_BAD = 2;
    const REVIEW_SCORE_NORMAL = 3;
    const REVIEW_SCORE_GOOD = 4;
    const REVIEW_SCORE_EXCELLENT = 5;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Review the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'review';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'evaluation, ratio, location, purity, comfort, services, personnel, safety, economy, features, appearance, kitchen, assortment, situation, service',
                'numerical',
                'integerOnly' => true
            ),
            array('entry_id', 'length', 'max' => 11),
            array('subject', 'length', 'max' => 2),
            array('title', 'length', 'max' => 255),
            array(
                'entry_id, subject, title, evaluation, ratio, location, purity, comfort, services, personnel, safety, economy, features, appearance, kitchen, assortment, situation, service',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'entry_id' => 'Entry',
            'subject' => 'Subject',
            'title' => 'Title',
            'scale' => 'Scale',
            'ratio' => 'Соотношение цена/качество',
            'location' => 'Расположение',
            'purity' => 'Purity',
            'comfort' => 'Комфорт',
            'services' => 'Services',
            'personnel' => 'Personnel',
            'safety' => 'Безопасность',
            'economy' => 'Экономичность',
            'features' => 'Ходовые качества',
            'appearance' => 'Внешний вид',
            'kitchen' => 'Кухня',
            'assortment' => 'Ассортимент',
            'situation' => 'Обстановка',
            'service' => 'Обслуживание',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('entry_id', $this->entry_id, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('evaluation', $this->evaluation);
        $criteria->compare('ratio', $this->ratio);
        $criteria->compare('location', $this->location);
        $criteria->compare('purity', $this->purity);
        $criteria->compare('comfort', $this->comfort);
        $criteria->compare('services', $this->services);
        $criteria->compare('personnel', $this->personnel);
        $criteria->compare('safety', $this->safety);
        $criteria->compare('economy', $this->economy);
        $criteria->compare('features', $this->features);
        $criteria->compare('appearance', $this->appearance);
        $criteria->compare('kitchen', $this->kitchen);
        $criteria->compare('assortment', $this->assortment);
        $criteria->compare('situation', $this->situation);
        $criteria->compare('service', $this->service);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @param null $id
     * @return array
     */
    public static function getEvaluationList($id = null) {

        $list = array(
            self::REVIEW_EVALUATION_NEUTRAL => 'Нейтральный отзыв',
            self::REVIEW_EVALUATION_GOOD => 'Хороший отзыв',
            self::REVIEW_EVALUATION_BAD => 'Отрицательный отзыв'
        );

        return ($id)? $list[$id]: $list;
    }

    /**
     * @param null $id
     * @return array
     */
    public static function getReviewTypesList($id = null) {

        $list = array(
            self::REVIEW_TYPE_WITHOUT_TYPE => 'Прочее',
            self::REVIEW_TYPE_CAR => 'Автомобиль',
            self::REVIEW_TYPE_HOTEL => 'Отель',
            self::REVIEW_TYPE_RESTAURANT => 'Ресторан'
        );

        return ($id)? $list[$id]: $list;
    }

    /**
     * @param null $id
     * @return array
     */
    public static function getScoreList($id = null) {

        $list = array(
            self::REVIEW_SCORE_TERRIBLE => 'Ужасно',
            self::REVIEW_SCORE_BAD => 'Плохо',
            self::REVIEW_SCORE_NORMAL => 'Нормально',
            self::REVIEW_SCORE_GOOD => 'Хорошо',
            self::REVIEW_SCORE_EXCELLENT => 'Отлично'
        );

        return ($id !== null)? $list[$id]: $list;
    }
}