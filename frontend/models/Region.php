<?php

/**
 * Class Author
 *
 * Класс для работы с сущностью "Регион"
 *
 * @property string $id
 * @property string $name
 * @property string $title
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Region extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Author the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'region';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
        );
    }
}