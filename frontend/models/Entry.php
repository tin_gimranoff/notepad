<?php

/**
 * Class Entry
 *
 * Class for processing entries
 *
 * @property string $id
 * @property string $alias
 * @property string $folder
 * @property string $title
 * @property string $text
 * @property string $author
 * @property integer $rating
 * @property integer $type
 * @property integer $published
 * @property integer $privacy
 * @property string $date_created
 * @property string $date_updated
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Entry extends ActiveRecord
{
    const ENTRY_TYPE_NORMAL = 1;
    const ENTRY_TYPE_DREAM = 2;
    const ENTRY_TYPE_PREDICTION = 3;
    const ENTRY_TYPE_REVIEW = 4;

    public static $moduleKey = MODULE_KEY_ENTRY;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Entry the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'entry';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('type', 'default', 'value' => 'dream'),
            array('text', 'required'),
            array('rating, type, published', 'numerical', 'integerOnly' => true),
            array('alias, title', 'length', 'max' => 255),
            array('folder, author, date_created, date_updated', 'length', 'max' => 11),
            array(
                'id, alias, folder, title, text, author, rating, type, published, date_created, date_updated',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'alias' => 'Alias',
            'folder' => 'Directory',
            'title' => 'Название записи',
            'text' => 'Text',
            'author' => 'Данные автора',
            'rating' => 'Rating',
            'type' => 'Тип записи',
            'published' => 'Status',
            'privacy' => 'Приватность',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('folder', $this->folder, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('author', $this->author, true);
        $criteria->compare('rating', $this->rating);
        $criteria->compare('type', $this->type);
        $criteria->compare('published', $this->published);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Get list of types of entries
     * @param null $id
     * @return array
     */
    public static function getTypeList($id = null)
    {

        $list = array(
            self::ENTRY_TYPE_NORMAL => 'Запись',
            self::ENTRY_TYPE_DREAM => 'Сон',
            self::ENTRY_TYPE_PREDICTION => 'Предсказание',
            self::ENTRY_TYPE_REVIEW => 'Отзыв'
        );

        return ($id) ? $list[$id] : $list;
    }

    /**
     * Get list of available privacy values
     * @return array
     */
    public static function getPrivacyList()
    {

        $list = array(
            'nothing' => 'Никому',
            'me' => 'Мне',
            'all' => 'Всем',
            'alternates' => 'Для выбранных пользователей',
        );

        return $list;
    }

    /**
     * Trigger "beforeSave"
     * @return bool
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->date_created = time();
        }

        $this->date_updated = time();

        return parent::beforeSave();
    }

    /**
     * Get list of entries by folder id
     * @param $folderId
     * @return array|bool
     */
    public static function getEntriesByFolder($folderId)
    {
        if (!$folderId) {
            return false;
        }

        $command = command();
        $command
            ->select()
            ->from(self::tableName())
            ->where("folder=$folderId");

        $rows = $command->queryAll();

        return $rows;
    }

    /**
     * Trigger "afterSave"
     */
    public function afterSave()
    {
        if (Yii::app()->hasComponent('user') && Yii::app()->user->id) {
            // Get id of current authorized user
            $userId = Yii::app()->user->id;

        }

        /*

        // Select array of subscribers of current user
        $rows = command()
            ->select('user_id')
            ->from(Subscribe::staticTableName())
            ->where("object_id=" . $this->id . " AND module_key=" . self::$moduleKey)
            ->queryAll();

        $subscribers = array();
        foreach ($rows as $row) {
            $subscribers[] = $row['user_id'];
        }

        // Add notification
        Notification::add();

        // Update date viewed for this entry
        command()->update(
            Subscribe::staticTableName(),
            array(
                'date_viewed' => 0
            ),
            array(
                'module_key' => MODULE_KEY_ENTRY,
                'object_id' => $this->id
            )
        );

        */
        parent::afterSave();
    }
}