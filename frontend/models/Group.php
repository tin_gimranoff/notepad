<?php

/**
 * Class Group
 *
 * Class for work with groups of contacts
 *
 * @property string $id
 * @property string $title
 * @property string $owner_id
 * @property string $contact_count
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Group extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Group the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title', 'length', 'max' => 255),
            array('owner_id', 'length', 'max' => 11),
            array('contact_count', 'length', 'max' => 5),
            array('id, title, owner_id, contact_count', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'owner_id' => 'Owner',
            'contact_count' => 'Contact Count',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('owner_id', $this->owner_id, true);
        $criteria->compare('contact_count', $this->contact_count, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getGroupTitle($id) {

        /**
         * @var $model Group
         */
        $model = self::model()->findByPk($id);

        return $model->title;
    }
}