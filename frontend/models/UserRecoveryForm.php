<?php

class UserRecoveryForm extends CFormModel
{

    public $login_or_email;
    public $user_id;

    public function rules()
    {
        return array(
            array('login_or_email', 'required'),
            array(
                'login_or_email',
                'match',
                'pattern' => '/^[A-Za-z0-9@.-\s,]+$/u',
                'message' => 'не правильный формат Email',
            ),
            array('login_or_email', 'checkexists'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'login_or_email' => 'Email или логин',
        );
    }

    public function checkexists($attribute, $params)
    {
        if (!$this->hasErrors()) // we only want to authenticate when no input errors
        {
            if (strpos($this->login_or_email, "@")) {
                $user = User::model()->findByAttributes(array('email' => $this->login_or_email));
                if ($user) {
                    $this->user_id = $user->id;
                }
            } else {
                $user = User::model()->findByAttributes(array('username' => $this->login_or_email));
                if ($user) {
                    $this->user_id = $user->id;
                }
            }

            if ($user === null) {
                if (strpos($this->login_or_email, "@")) {
                    $this->addError("login_or_email", "Не правильно введен Email.");
                } else {
                    $this->addError("login_or_email", "Не верный логин.");
                }
            }
        }
    }

}