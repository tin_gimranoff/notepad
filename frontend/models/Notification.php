<?php

/**
 * Class Notification
 *
 * Class for processing notifications for users
 *
 * @property string $id
 * @property string $user_id
 * @property integer $module_key
 * @property string $title
 * @property string $text
 * @property integer $status
 * @property integer $email
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_sended
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Notification extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Notification the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'notification';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('text', 'required'),
            array('module_key, status, email', 'numerical', 'integerOnly' => true),
            array('user_id, date_created, date_updated, date_sended', 'length', 'max' => 11),
            array('title', 'length', 'max' => 255),
            array(
                'id, user_id, module_key, title, text, status, email, date_created, date_updated, date_sended',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'module_key' => 'Module Key',
            'title' => 'Title',
            'text' => 'Text',
            'status' => 'Status',
            'email' => 'Email',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'date_sended' => 'Date Sended',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('module_key', $this->module_key);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('email', $this->email);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('date_sended', $this->date_sended, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function add()
    {
        $notification = new Notification();
        $notification->text = 'lalala';
        $notification->user_id = 2;
        return $notification->save();
    }
}