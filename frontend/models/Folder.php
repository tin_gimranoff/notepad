<?php

/**
 * Class Folder
 *
 * Модель для работы с папками
 *
 * @property string $id
 * @property string $parent_id
 * @property string $user_id
 * @property string $name
 * @property string $title
 * @property string $entry_count
 * @property integer $active
 * @property integer $privacy
 * @property string $order
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Folder extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Folder the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'folder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('active, privacy', 'numerical', 'integerOnly' => true),
			array('parent_id, user_id, order', 'length', 'max' => 11),
			array('name, title', 'length', 'max' => 255),
			array('entry_count', 'length', 'max' => 5),
			array('id, parent_id, user_id, name, title, entry_count, active, privacy, order', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'user_id' => 'User',
			'name' => 'Name',
			'title' => 'Title',
			'entry_count' => 'Entry Count',
			'active' => 'Active',
			'privacy' => 'Privacy',
			'order' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('parent_id', $this->parent_id, true);
		$criteria->compare('user_id', $this->user_id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('entry_count', $this->entry_count, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('privacy', $this->privacy);
		$criteria->compare('order', $this->order, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function relations()
	{
		return array(
			//'entries' => array(self::HAS_MANY,'Entry',module_id,condition => "module_key=" . self::module_key)
		);
	}

	/**
	 * Trigger "beforeSave"
	 * @return bool
	 * TODO Add checking uniqueness of name. If name is not unique - add number to tail
	 */
	public function beforeSave()
	{

		return parent::beforeSave();
	}
}