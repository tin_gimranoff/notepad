<?php

/**
 * Class Place
 *
 * Класс для работы с местоположением объекта
 *
 * @property string $id
 * @property string $zip
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $address
 * @property string $coordx
 * @property string $coordy
 * @property string $title
 * @property string $date_created
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Place extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Placement the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'placement';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('zip', 'length', 'max' => 8),
            array('country', 'length', 'max' => 3),
            array('region, city', 'length', 'max' => 5),
            array('address', 'length', 'max' => 50),
            array('coordx, coordy', 'length', 'max' => 10),
            array('title', 'length', 'max' => 255),
            array('date_created', 'length', 'max' => 11),
            array(
                'id, zip, country, region, city, address, coordx, coordy, title, date_created',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'zip' => 'Zip',
            'country' => 'Country',
            'region' => 'Region',
            'city' => 'City',
            'address' => 'Address',
            'coordx' => 'Coordx',
            'coordy' => 'Coordy',
            'title' => 'Title',
            'date_created' => 'Date Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('zip', $this->zip, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('region', $this->region, true);
        $criteria->compare('city', $this->city, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('coordx', $this->coordx, true);
        $criteria->compare('coordy', $this->coordy, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('date_created', $this->date_created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getPlaceList() {

        $list = array();

        return $list;
    }

    public function getCurrentCity($ip = NULL)
    {
        $postData = "
        <ipquery>
            <fields>
                <all/>
            </fields>
            <ip-list>
                <ip>$ip</ip>
            </ip-list>
        </ipquery>
    ";

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'http://194.85.91.253:8090/geo/geo.html');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $responseXml = curl_exec($curl);
        curl_close($curl);

        if (substr($responseXml, 0, 5) == '<?xml') {
            $ipinfo = new SimpleXMLElement($responseXml);
            //echo $ipinfo->ip->city; // город
            //echo $ipinfo->ip->region; // регион
            //echo $ipinfo->ip->district; // федеральный округ РФ     
            return $ipinfo->ip;
        }
        return false;
    }
}