<?php

/**
 * Class User
 *
 * Model for work this users
 *
 * @property string $id
 * @property string $alias
 * @property string $email
 * @property string $password
 * @property integer $status
 * @property string $login
 * @property string $alter_email
 * @property integer $alter_email_v
 * @property string $name
 * @property string $surname
 * @property integer $surname_v
 * @property string $patronymic
 * @property integer $patronymic_v
 * @property string $skype
 * @property integer $skype_v
 * @property string $icq
 * @property integer $icq_v
 * @property string $phone1
 * @property integer $phone1_v
 * @property string $phone2
 * @property integer $phone2_v
 * @property string $birthday
 * @property integer $birthday_v
 * @property integer $space_usage
 * @property string $date_created
 * @property string $date_updated
 * @property string $last_login
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class User extends ActiveRecord
{
    public static $moduleKey = MODULE_KEY_USER;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'status, alter_email_v, surname_v, patronymic_v, skype_v, icq_v, phone1_v, phone2_v, birthday_v',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'alias, email, login, alter_email, name, surname, patronymic, skype, icq, phone1, phone2',
                'length',
                'max' => 255
            ),
            array('password', 'length', 'max' => 32),
            array('birthday, date_created, date_updated, last_login', 'length', 'max' => 11),
            array(
                'id, alias, email, password, status, login, alter_email, alter_email_v, name, surname, surname_v, patronymic, patronymic_v, skype, skype_v, icq, icq_v, phone1, phone1_v, phone2, phone2_v, birthday, birthday_v, date_created, date_updated, last_login',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'alias' => 'Alias',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'login' => 'Логин пользователя',
            'alter_email' => 'Alter Email',
            'alter_email_v' => 'Alter Email V',
            'name' => 'Name',
            'surname' => 'Surname',
            'surname_v' => 'Surname V',
            'patronymic' => 'Patronymic',
            'patronymic_v' => 'Patronymic V',
            'skype' => 'Skype',
            'skype_v' => 'Skype V',
            'icq' => 'Icq',
            'icq_v' => 'Icq V',
            'phone1' => 'Phone1',
            'phone1_v' => 'Phone1 V',
            'phone2' => 'Phone2',
            'phone2_v' => 'Phone2 V',
            'birthday' => 'Birthday',
            'birthday_v' => 'Birthday V',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'last_login' => 'Last Login',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('alter_email', $this->alter_email, true);
        $criteria->compare('alter_email_v', $this->alter_email_v);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('surname_v', $this->surname_v);
        $criteria->compare('patronymic', $this->patronymic, true);
        $criteria->compare('patronymic_v', $this->patronymic_v);
        $criteria->compare('skype', $this->skype, true);
        $criteria->compare('skype_v', $this->skype_v);
        $criteria->compare('icq', $this->icq, true);
        $criteria->compare('icq_v', $this->icq_v);
        $criteria->compare('phone1', $this->phone1, true);
        $criteria->compare('phone1_v', $this->phone1_v);
        $criteria->compare('phone2', $this->phone2, true);
        $criteria->compare('phone2_v', $this->phone2_v);
        $criteria->compare('birthday', $this->birthday, true);
        $criteria->compare('birthday_v', $this->birthday_v);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('last_login', $this->last_login, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}