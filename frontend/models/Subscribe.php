<?php

/**
 * Class Follower
 *
 * Model for process subscribers
 *
 * @property string $user_id
 * @property integer $module_key
 * @property string $object_id
 * @property string $date_viewed
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Subscribe extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Subscribe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subscribe';
	}

    public static function staticTableName()
    {
        return 'subscribe';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('module_key', 'numerical', 'integerOnly'=>true),
			array('user_id, object_id, date_viewed', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, module_key, object_id, date_viewed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'module_key' => 'Module Key',
			'object_id' => 'Object',
			'date_viewed' => 'Date Viewed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('module_key',$this->module_key);
		$criteria->compare('object_id',$this->object_id,true);
		$criteria->compare('date_viewed',$this->date_viewed,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}