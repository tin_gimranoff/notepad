<?php

/**
 * Class Dream
 *
 * Класс для работы с дополнительными полями записей типа "Сон"
 *
 * @property string $entry_id
 * @property string $tags
 * @property integer $scale
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Dream extends ActiveRecord
{
    const DREAM_NEUTRAL = 1;
    const DREAM_BAD = 2;
    const DREAM_GOOD = 3;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Dream the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'dream';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('tags', 'required'),
            array('scale', 'numerical', 'integerOnly' => true),
            array('entry_id', 'length', 'max' => 11),
            array('entry_id, tags, scale', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'entry_id' => 'Entry',
            'tags' => 'Что снилось?',
            'scale' => 'Scale',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('entry_id', $this->entry_id, true);
        $criteria->compare('tags', $this->tags, true);
        $criteria->compare('scale', $this->scale);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Getting dream characteristics
     * @param null $id
     * @return array
     */
    public static function getScaleList($id = null)
    {

        $list = array(
            self::DREAM_GOOD => 'Хороший сон',
            self::DREAM_BAD => 'Плохой сон',
            self::DREAM_NEUTRAL => 'Нейтральный сон'
        );

        return ($id)? $list[$id]: $list;
    }
}