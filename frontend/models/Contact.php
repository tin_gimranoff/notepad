<?php

/**
 * Class Contact
 *
 * Class for work with user contacts
 *
 * @property string $id
 * @property string $owner_id
 * @property string $user_id
 * @property string $contact_email
 * @property string $title
 * @property string $group_id
 * @property integer $date_created
 * @property integer $date_updated
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Contact extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Contact the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'contact';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('owner_id, user_id, group_id', 'length', 'max' => 11),
            array('email', 'email'),
            array('title', 'length', 'max' => 255),
            array('id, owner_id, user_id, title, group_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'owner_id' => 'Owner',
            'user_id' => 'User',
            'title' => 'Title',
            'group_id' => 'Group',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('owner_id', $this->owner_id, true);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('group_id', $this->group_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {

        if ($this->isNewRecord) $this->date_created = time();

        $this->date_updated = time();

        return parent::beforeSave();
    }
}