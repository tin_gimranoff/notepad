<?php

/**
 * Class Prediction
 *
 * Класс для работы с записями типа "Предсказание"
 *
 * @property string $entry_id
 * @property string $destination
 * @property string $event_date
 * @property string $date_publicated
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @verson 1.0
 */
class Prediction extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Prediction the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'prediction';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('entry_id, event_date, date_publicated', 'length', 'max' => 11),
            array('destination', 'length', 'max' => 255),
            array('entry_id, destination, event_date, date_publicated', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'entry_id' => 'Entry',
            'destination' => 'Destination',
            'event_date' => 'Event Date',
            'date_publicated' => 'Date Publicated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('entry_id', $this->entry_id, true);
        $criteria->compare('destination', $this->destination, true);
        $criteria->compare('event_date', $this->event_date, true);
        $criteria->compare('date_publicated', $this->date_publicated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}