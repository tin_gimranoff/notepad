<?php

class LoginForm extends CFormModel
{
    public $email;
    public $password;
    //public $remember;

    /**
     * Declares the validation rules.
     * The rules state that email and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('email, password', 'required'),
            //array('remember', 'boolean'),
            array('password', 'authenticate'),
        );
    }

    public function attributeLabels()
    {
        return array(
            //'remember' => 'asd',
            'email' => 'Логин',
            'password' => 'Пароль',
        );
    }

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $identity = new UserIdentity($this->email, $this->password);
            $identity->authenticate();

            switch ($identity->errorCode) {
                case UserIdentity::ERROR_NONE:
                    //$duration = $this->remember ? Yii::app()->controller->module->rememberTime : 0;
                    Yii::app()->user->login($identity);
					$user = User::model()->findByAttributes(array('email' => $this->email));
					$user->last_login = time();
					$user->save();
                    break;
                case UserIdentity::ERROR_EMAIL_INVALID:
                    $this->addError("email", "Не верные логин или пароль.");
                    break;

                case UserIdentity::ERROR_STATUS_NOTACTIV:
                    $this->addError("status", "Аккаунт неактивен.");
                    break;

                case UserIdentity::ERROR_STATUS_BAN:
                    $this->addError("status", "Аккаунт заблокирован.");
                    break;

                case UserIdentity::ERROR_PASSWORD_INVALID:
                    $this->addError("password", "Не верные логин или пароль.");
                    break;
            }
        }
    }
}
