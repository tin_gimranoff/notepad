<?php

/**
 * Class RegistrationForm
 *
 * Model for registration form
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class RegistrationForm extends User
{
    //public $verifyPassword;
    //public $verifyCode;

    public function rules()
    {
        $rules = array(
            array('password, email', 'required'),
            array(
                'password',
                'length',
                'max' => 128,
                'min' => 4,
                'message' => "Минимальное количество символов - 4"
            ),
            array('email', 'email'),
            array('email', 'unique', 'message' => "Такой email уже существует"),
        );

        return $rules;
    }
}