<?php

/**
 * Class Feedback
 *
 * Model for process feedback data
 *
 * @property string $id
 * @property string $username
 * @property string $usermail
 * @property string $subject
 * @property string $text
 * @property string $date_created
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class Feedback extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Feedback the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'feedback';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('usermail', 'required'),
            array('username, usermail, subject', 'length', 'max' => 255),
            array('date_created', 'length', 'max' => 11),
            array('text', 'safe'),
            array('id, username, usermail, subject, text, date_created', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'usermail' => 'Usermail',
            'subject' => 'Subject',
            'text' => 'Text',
            'date_created' => 'Date Created',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('usermail', $this->usermail, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('text', $this->text, true);
        $criteria->compare('date_created', $this->date_created, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {

        if ($this->isNewRecord) {
            $this->date_created = time();
        }

        return parent::beforeSave();
    }
}