<?php $form = $this->beginWidget('ActiveForm', array(
        'id'=>'feedback-form',
        'enableAjaxValidation'=>false,
    )); ?>

<?php echo $form->errorSummary($model); ?>

    <div class="form-row span4">
        <?php echo CHtml::label('Представьтесь пожалуйста','username'); ?>
        <?php echo $form->textField($model, 'username'); ?>
    </div>
    <div class="form-row span4">
        <?php echo CHtml::label('Введите тему письма','subject'); ?>
        <?php echo $form->textField($model, 'subject'); ?>
    </div>
    <div class="form-row span4">
        <?php echo CHtml::label('Введите ваш E-mail','usermail',array('required'=>true)); ?>
        <?php echo $form->textField($model, 'usermail'); ?>
    </div>
    <div class="form-row span12">
        <?php echo CHtml::label('Текст письма','text',array('required'=>true)); ?>
        <?php echo $form->textArea($model, 'text',array('cols'=>20,'rows'=>10,'maxlength'=>1500)); ?>
    </div>
    <div class="form-row">
        <div class="text-length">Осталось знаков: <span>1500</span></div>
        <div class="buttonHolder">
            <?php echo CHtml::submitButton('Отправить собщение', array('class'=>"blueButtonS")); ?>
    </div>

<?php $this->endWidget(); ?>