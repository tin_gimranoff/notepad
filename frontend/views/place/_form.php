<?php
/* @var $this PlaceController */
/* @var $model Place */
/* @var $form ActiveForm */
?>
<?php

clientScript()->registerScript('dialog-map', '
    ymaps.ready(init);
    var dialogMap,
        currentPlacemark;

    function init(){
        dialogMap = new ymaps.Map ("dialogMap", {
            center: [55.76, 37.64],
            zoom: 7
        });

        // Создание экземпляра элемента управления
        dialogMap.controls.add(
            new ymaps.control.ZoomControl()
        );
    }

    ymaps.ready(function() {

    	var p = new ymaps.Placemark([59.9385, 30.313497],{}, {draggable: true});

    	p.events.add("drag", function (e) {
    		//console.log(e.get("target").geometry.getCoordinates());
		});

		p.events.add("dragend", function (e) {
    		console.log(e.get("target").geometry.getCoordinates());
		});

    	dialogMap.geoObjects.add(p);


    })
');

clientScript()->registerCss('dialog-map', '#dialogMap { height: 400px; width: 100%; }');

?>
<?php $form=$this->beginWidget('ActiveForm', array(
	'id'=>'placement-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div>
		<?php echo $form->labelEx($model,'region'); ?>
		<?php echo $form->dropDownList($model,'region',Region::getRegions(),array()); ?>
		<?php echo $form->error($model,'region'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

    <div id="dialogMap" class="yMap"></div>

	<div>
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>



	<div>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
