<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
    ymaps.ready(init);
    var placesMap,
        currentPlacemark;


    function init(){
        placesMap = new ymaps.Map ("map", {
            center: [55.76, 37.64],
            zoom: 7
        });

        // Создание экземпляра элемента управления
        placesMap.controls.add(
            new ymaps.control.ZoomControl()
        );

        pCount = places.length;
        //console.log(places);

        if (pCount) {

            for (var i = 0; i < pCount; i++) {

                currentPlacemark = new ymaps.Placemark([places[i].x, places[i].y], {
                    content: places[i].content,
                    balloonContent: 'вфывфы',
                    iconContent: i
                });

                placesMap.geoObjects.add(currentPlacemark);
            }
        }

        // Поставим метку по клику над картой
        placesMap.events.add('click', function (e) {
            // Географические координаты точки клика можно узнать
            // посредством вызова .get('coordPosition')
            var position = e.get('coordPosition');
            placesMap.geoObjects.add(new ymaps.Placemark(position), {draggable:true});
        });
    }


</script>
<style>
    #map { height: 400px; width: 100%; }
</style>
<div class="row-fluid wrapperGroupHolder">
    <div class="span12 headerGroup">
        <div class="groupLeft">
            <h1>Мои места</h1>
            <ul class="wrapperGroup">
                <li class="expansionGroup active"><a href="#">Списком</a></li>
                <li class="listPlaces"><a href="#">На карте</a></li>
            </ul>
        </div>
        <div class="groupRight">
            <div class="buttonHolder">
				<?php echo CHtml::link('Добавить новое место', '#myModal', array('class'=>'blueButton','data-toggle'=>"modal")); ?>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid wrapperGroupContentHolder normal">
    <div class="span12 simpleGroupHolder">
        <ul class="placesList">
            <li>
                <?php echo $this->renderPartial('_inner_place_row'); ?>
            </li>
        </ul>
    </div>
</div>
<div class="row-fluid wrapperGroupContentHolder extended">
    <div class="span12 simpleGroupHolder">
        <div id="map" class="yMap"></div>
    </div>
</div>
<div>
    <?php $this->renderPartial('create',array('model'=>$placement)); ?>
</div>