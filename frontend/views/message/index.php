<div class="row-fluid wrapperGroupHolder">
    <div class="span12 headerGroup">
        <div class="groupLeft">
            <h1>Сообщения</h1>
            <ul class="wrapperGroup">
                <li class="active"><a href="#">Все сообщения</a></li>
                <li><a href="#">Новые</a></li>
                <li><a href="#">Входящие</a></li>
                <li><a href="#">Исходящие</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row-fluid wrapperGroupContentHolder">
    <div class="span12 messageHolder">
        <ul class="messageList">
            <li class="not-read">
                <div class="mCheck">
                    <div class="checkbox-holder">
                        <span><input class="checkBox" type="checkbox" name="visibleall"/></span>
                    </div>
                </div>
                <div class="avatar">
                    <div class="avatar-image size-20"><img src="img/content/avatar.jpg" alt=""></div>
                </div>
                <span class="mTheme">
                    <a href="#">Тема сообщения</a>
                </span>
                <span class="mShort">
                    Привет. Написал Тебе это все еще в понедельник, но увы, только сейчас получается отправить..
                </span>
                <span class="mDate">
                    22.01
                </span>
                <span class="fileIco">
                    <img src="img/elem/fileIco.png" alt="">
                </span>
            </li>
            <li class="">
                <div class="mCheck">
                    <div class="checkbox-holder">
                        <span><input class="checkBox" type="checkbox" name="visibleall"/></span>
                    </div>
                </div>
                <div class="avatar">
                    <div class="avatar-image size-20"><img src="img/content/avatar.jpg" alt=""></div>
                </div>
                <span class="mTheme">
                    <a href="#">Что-то важное</a>
                </span>
                <span class="mShort">
                    This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                </span>
                <span class="mDate">
                    22.01
                </span>
                <span class="fileIco">
                    <img src="img/elem/fileIco.png" alt="">
                </span>
            </li>
            <li class="">
                <div class="mCheck">
                    <div class="checkbox-holder">
                        <span><input class="checkBox" type="checkbox" name="visibleall"/></span>
                    </div>
                </div>
                <div class="avatar">
                    <div class="avatar-image size-20"><img src="img/content/avatar.jpg" alt=""></div>
                </div>
                <span class="mTheme">
                    <a href="#">Ещё одна какая-то тема</a>
                </span>
                <span class="mShort">
                    Morbi accumsan ipsum velit. Nam nec tellus a odio tinciduntThis is Photoshop's version  of Lorem Ipsum
                </span>
                <span class="mDate">
                    22.01
                </span>
                <span class="fileIco"></span>
            </li>
        </ul>
        <div class="blockRow">
            <strong>Операции с выбранными:</strong>
            <div class="buttonHolder">
                <a href="#">Отменить как прочитанное</a>
            </div>
            <div class="buttonHolder">
                <a href="#">Отменить как не прочитанное</a>
            </div>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </div>
    </div>
</div>