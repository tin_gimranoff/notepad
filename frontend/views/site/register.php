<h1>Регистрация</h1>

<?php if (Yii::app()->user->hasFlash('registration')): ?>
    <div class="success">
        <?php echo Yii::app()->user->getFlash('registration'); ?>
    </div>
<?php else: ?>
    <div style="padding-left: 40px">
        <?php $form = $this->beginWidget('ActiveForm', array(
                'id' => 'registration-form',
                'enableAjaxValidation' => true,
                //'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
            )
        ); ?>

        <p class="note"><?php echo 'Fields with <span class="required">*</span> are required.'; ?></p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email'); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password'); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>

        <?php if (Helper::doCaptcha('registration')): ?>
            <div class="">
                <?php echo $form->labelEx($model, 'verifyCode'); ?>

                <?php $this->widget('CCaptcha'); ?>
                <?php echo $form->textField($model, 'verifyCode'); ?>
                <?php echo $form->error($model, 'verifyCode'); ?>

                <p class="hint"><?php echo "Please enter the letters as they are shown in the image above."; ?>
                    <br/><?php echo "Letters are not case-sensitive."; ?></p>
            </div>
        <?php endif; ?>

        <div class="row submit">
            <?php echo CHtml::submitButton("Register"); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
<?php endif; ?>