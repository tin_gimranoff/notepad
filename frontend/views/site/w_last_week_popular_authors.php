<h2 class="block-title">Лучшие авторы недели в <a href="#" class="city-link">Москве</a></h2>
<div class="block-container authors-block">
    <div class="block-row">
        <img src="img/content/author1.jpg" alt="">
        <a href="#" class="subscribed">Имя Фамилия Отчество</a><a href=""><span class="with-suscribe"></span></a>
        <p class="quote">Партий много, все оппозиционные, про-правительственных нету, все критикуют правительство, президента, страну, так сказать, а где позитив? А позитива нет, позитив — только у ЛДПР.</p>
        <p class="rating">Рейтинг: <strong>Очень высокий (4537.4)</strong></p>
    </div>
    <div class="block-row">
        <img src="img/content/author2.jpg" alt="">
        <a href="#" class="with-suscribe not-subscribed">Длинноеимя Длиннаяфамилия Длинноеотчество</a>
        <p class="quote">Есть и пить нужно столько, чтобы наши силы этим восстанавливались, а не подавлялись. <br>Марк Тулий Цицерон</p>
        <p class="rating">Рейтинг: <strong>Низкий (32.4)</strong></p>
    </div>
    <div class="block-row">
        <img src="img/content/author3.jpg" alt="">
        <a href="#" class="with-suscribe subscribed">Имя Фамилия Отчество</a>
        <p class="quote">Партий много, все оппозиционные, про-правительственных нету, все критикуют правительство, президента, страну, так сказать, а где позитив? А позитива нет, позитив — только у ЛДПР.</p>
        <p class="rating">Рейтинг: <strong>Очень высокий (4537.4)</strong></p>
    </div>
</div>