<?php
/**
 * @var $model LoginForm
 */
?>
<h1>Авторизация</h1>

<?php if (Yii::app()->user->hasFlash('loginMessage')) : ?>
<div class="success">
    <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>
<?php endif; ?>

<div style="padding-left: 40px">
    <?php echo CHtml::beginForm(); ?>
    <?php echo CHtml::errorSummary($model); ?>

    <div>
        <?php echo CHtml::activeLabelEx($model,'email'); ?>
        <?php echo CHtml::activeTextField($model,'email') ?>
    </div>

    <div>
        <?php echo CHtml::activeLabelEx($model,'password'); ?>
        <?php echo CHtml::activePasswordField($model,'password') ?>
    </div>

    <div>
        <p class="hint">
            <?php echo CHtml::link('Регистрация',Yii::app()->user->registerUrl); ?> |
            <?php echo CHtml::link('Забыли пароль?',Yii::app()->user->recoveryUrl); ?>
        </p>
    </div>

    <div class="remember">
        <?php echo CHtml::activeCheckBox($model,'remember'); ?>
        <?php echo CHtml::activeLabelEx($model,'remember'); ?>
    </div>

    <div class="submit">
        <?php echo CHtml::submitButton("Login"); ?>
    </div>

    <?php echo CHtml::endForm(); ?>
</div><!-- form -->