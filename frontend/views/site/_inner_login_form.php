<?php
/**
 * @var $this SiteController
 * @var $form ActiveForm
 */
?>

<div class="form">
    <?php $form = $this->beginWidget('ActiveForm',array(
            'id'=>'inner-login-form',
            'action'=>url('site/login')
        )); ?>

    <h4>Войти в свой аккаунт</h4>

    <div class="row">
        <?php echo $form->textField($model,'username',array('value'=>'Логин или Email','class'=>"autoclear")) ?>
    </div>

    <div class="row">
        <?php echo $form->passwordField($model,'password',array('value'=>'Пароль','class'=>'autoclear')); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::link('Забыли пароль?'); ?>
        <?php echo CHtml::submitButton("Войти"); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->