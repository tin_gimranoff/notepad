<div class="form">
    <?php $form = $this->beginWidget('ActiveForm', array(
            'action'=>app()->createUrl('site/register'),
            'htmlOptions' => array('enctype' => 'multipart/form-data'),
        )
    ); ?>

    <h4>Быстрая регистрация</h4>
    <?php echo $form->errorSummary(array($model)); ?>

    <div class="row">
        <?php echo $form->textField($model, 'username'); ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->passwordField($model, 'password'); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::link('Полная форма', array('register')); ?>
        <?php echo CHtml::submitButton("Зарегистрироваться"); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->