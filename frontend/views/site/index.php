<?php
/**
 * @var $this SiteController
 */
?>

<div class="main-page-slider">
    <ul>
        <li>
            <img src="img/content/slide-img1.png" alt="">
            <div>
                <h2>Заведите свой личный блокнот</h2>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
            </div>
        </li>
        <li>
            <img src="img/content/slide-img2.png" alt="">
            <div>
                <h2>Записывайте свои сны и толкования</h2>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
            </div>
        </li>
        <li>
            <img src="img/content/slide-img3.png" alt="">
            <div>
                <h2>Сохраняйте предсказания</h2>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
            </div>
        </li>
        <li>
            <img src="img/content/slide-img4.png" alt="">
            <div>
                <h2>Оставляйте отзвывы</h2>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
            </div>
        </li>
        <li>
            <img src="img/content/slide-img5.png" alt="">
            <div>
                <h2>Создавайте и делитесь записями</h2>
                <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
            </div>
        </li>
    </ul>
</div><!--Slider End-->

<div class="row-fluid">
<div class="span8 main-content">
    <div class="content-block">
        <?php $this->widget('LastDayPopularEntries'); ?>
    </div>
    <div class="content-block">
        <?php $this->widget('LastWeekPopularAuthors'); ?>
    </div>
    <div class="row-fliud">
        <div class="span6">
            <div class="content-block">
                <?php $this->widget('LastNormalEntries'); ?>
            </div>
        </div>
        <div class="span6">
            <div class="content-block">
                <?php $this->widget('LastPredictions'); ?>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <div class="content-block">
                <?php $this->widget('LastDreams'); ?>
            </div>
        </div>
        <div class="span6">
            <div class="content-block">
                <?php $this->widget('LastReviews'); ?>
            </div>
        </div>
    </div>
</div>
<div class="span4 right-side">
    <div class="filter-block">
        <!-- <img src="img/elem/filter-bg-ie.png" alt=""> -->
        <h2>Фильтруйте содержимое легко!</h2>
        <strong>С помощью наших фильтров вы можете:</strong>
        <ul>
            <li>- Смотреть что обсуждают на вашей даче</li>
            <li>- О чем пишут рядом с вашей работой</li>
            <li>- Что сниться людям в Уфе последние дни</li>
            <li>- О ком хорошо отзываются в Рязани</li>
        </ul>
        <div class="button-holder">
            <a href="#" class="green-button">Воспользоваться фильтрами</a>
        </div>

    </div>
    <div class="subscribe-block">
        <h3>Центр подписки</h3>
        <p>Подписка является полностью бесплатной</p>
        <form action="#" class="subscribe-form">
            <fieldset>
                <div class="form-row"><label for="subscribe-field">Впишите ключевые слова новостей</label><input name="subscribe-field" id="subscribe-field" type="text" class="styled-input" value="Например, ваша фирма, интересующая тема..."></div>
            </fieldset>
        </form>
        <div class="button-holder">
            <a href="#" class="green-button">Оформить подписку</a>
        </div>

    </div>

    <div class="content-block">
        <?php $this->widget('LastComments'); ?>
    </div>
    <div class="make-short-block">
        <h4>Используйте короткую ссылку</h4>
        <p>Популяризируйте свой аккаунт</p>
        <div class="buttonHolder">
            <a href="#" class="white-button">Попробовать</a>
        </div>
    </div>
</div>
</div>