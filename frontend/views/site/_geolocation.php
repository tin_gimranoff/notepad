<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script type="text/javascript">
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init(){
        myMap = new ymaps.Map ("map", {
            center: [55.76, 37.64],
            zoom: 7
        });

        // Создание экземпляра элемента управления
        myMap.controls.add(
            new ymaps.control.ZoomControl()
        );

        myPlacemark = new ymaps.Placemark([55.76, 37.64], {
            content: 'Москва!',
            balloonContent: 'Столица России'
        });

        myMap.geoObjects.add(myPlacemark);
    }
</script>

<div>
    <?php echo CHtml::label('Выберите регион','region'); ?>
    <?php echo CHtml::dropDownList('region','',array(),array('prompt'=>'Москва')) ?>
</div>
<div id="map" style="margin: 0 auto;width: 900px; height: 300px"></div>