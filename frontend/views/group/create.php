<?php $this->beginWidget(
    'zii.widgets.jui.CJuiDialog',
    array(
        'id' => 'group-create',
        'options' => array(
            'title' => 'Добавить группу',
            'autoOpen' => false,
            'modal' => true,
            'resizable' => false,
            'width' => 600
        ),
    )
);

$this->renderPartial('_form', array('model' => $model));

$this->endWidget('zii.widgets.jui.CJuiDialog');