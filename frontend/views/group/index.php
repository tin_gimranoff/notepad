<?php
clientScript()->registerScript('load_contacts', '
$(function() {

    $(".contact_group").on("click", function() {

        var group = $(this).attr("group_id");

        $.ajax({
            url: "index.php?r=group/loadContacts",
            data: {group: group},
            dataType: "json",
            type: "POST"
        }).done(function(data) {


            $(".folder_entries_content").html(out);

            $(".folder_entries_content").sortable();
        });
    });
})
');
?>
<div class="row-fluid wrapperGroupHolder">
    <div class="span12 headerGroup">
        <div class="groupLeft">
            <h1>Группы</h1>
            <ul class="wrapperGroup">
                <li class="expansionGroup active"><a href="#">Упрощенный вид</a></li>
                <li class="simpleGroup"><a href="#">Расширенный вид</a></li>
            </ul>
        </div>
        <div class="groupRight">
            <div class="buttonHolder">
                <?php echo CHtml::link('Добавить группу', '#createGroup', array('data-toggle'=>"modal",'class'=>"blueButton")); ?>
            </div>
        </div>
    </div>
</div>

<div class="row-fluid wrapperGroupContentHolder extended tabs">
    <div class="span3 left-side">
        <?php if (sizeof($list)) : ?>
        <ul class="navGroupList">
            <?php foreach ($list as $key=>$row) : ?>
                <li><a class="contact_group" group_id="<?php echo $row->id; ?>" href="#tab<?php echo $key + 1; ?>"><span class="borderSpan"></span><?php echo $row->title; ?><span class="activeBorder"></span></a></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
        <div class="addButton">
            <?php echo CHtml::link('Добавить группу', '#createGroup', array('data-toggle'=>"modal")); ?>
        </div>
    </div>
    <div class="span9 main-content">
        <?php if (sizeof($list)) : ?>
            <?php foreach ($list as $key=>$row) : ?>
                <div class="groupListBlock clearfix"  id="tab<?php echo $key + 1; ?>">
                    <?php $this->renderPartial('_inner_extended_row', array('group'=>$row)); ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<div class="row-fluid wrapperGroupContentHolder normal">
    <div class="span12 simpleGroupHolder">
        <?php if (sizeof($list)) : ?>
            <?php foreach ($list as $row) : ?>
                <?php $this->renderPartial('_inner_short_row', array('group'=>$row)); ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<?php $this->renderPartial('createGroup',array('group'=>$group)); ?>
<?php $this->renderPartial('createContact',array('contact'=>$contact)); ?>