<?php
/* @var $this GroupController */
/* @var $model Group */
/* @var $form ActiveForm */
?>

<div class="form" style="padding-left: 30px;">
    <?php $form = $this->beginWidget(
        'ActiveForm',
        array(
            'id' => 'group-form',
            'enableAjaxValidation' => false,
            'action' => ($model->isNewRecord) ? Yii::app()->urlManager->createUrl('group/create') :
                Yii::app()->urlManager->createUrl('group/update')
        )
    ); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->