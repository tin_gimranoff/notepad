<div class="groupListBlock clearfix">
    <div class="logoGroup">
        <div class="headerImgHolderGroup">
            <img src="img/content/groupLogo.png" alt=""/>
            <span>группа</span>
        </div>
        <div class="button-holder">
            <?php echo CHtml::link('Добавить', '#createContact', array('data-toggle'=>"modal",'class'=>'green-button')); ?>
        </div>
    </div>
    <div class="headrGroup">
        <h1><?php echo $group->title; ?></h1>
        <div class="listDiskSpace-button">
            <a href="#">Удалить</a>
        </div>
        <div class="buttonHolder">
            <a href="<?php echo Yii::app()->urlManager->createUrl('group/update', array('id'=>$group->id)); ?>">Редактировать</a>
        </div>
    </div>
    <ul class="innerListGroup sortable">
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a href="#">Венглинский Дмитрий Сергеевич </a>Записей в совместном доступе: 18</p>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </li>
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a href="#">Василий Алибабаевич Пупкин </a>Записей в совместном доступе: 1</p>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </li>
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a href="#">operator8tv@gmail.com </a>Записей в совместном доступе: 42</p>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </li>
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a href="#">Варвара </a>Записей в совместном доступе: 7356</p>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </li>
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a href="#">mhaff@mail.ru </a>Записей в совместном доступе: 18</p>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </li>
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a href="#">Венглинский Дмитрий Сергеевич </a>Записей в совместном доступе: 18</p>
            <div class="listDiskSpace-button">
                <a href="#">Удалить</a>
            </div>
        </li>
    </ul>
</div>