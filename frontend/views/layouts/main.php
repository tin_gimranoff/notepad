<?php
/*var_dump(Yii::app()->session['city']);
var_dump(Yii::app()->session['lat']);
var_dump(Yii::app()->session['lng']);
var_dump(Yii::app()->session['address']);
var_dump(Yii::app()->session['region']);*/
if($_SERVER['REMOTE_ADDR'] != '127.0.0.1')
  $ip = $_SERVER['REMOTE_ADDR'];
else
  $ip = '217.15.199.140';
$ip_info = Place::model()->getCurrentCity($ip); 
if(empty(Yii::app()->session['city']) || Yii::app()->session['city'] != (string)$ip_info->city)
{
     Yii::app()->session['city'] = (string)$ip_info->city;
     Yii::app()->session['lat'] = (string)$ip_info->lat;
     Yii::app()->session['lng'] = (string)$ip_info->lng;
	   Yii::app()->session['address'] = '';
	   Yii::app()->session['region'] = '';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/nav.css">
    <link href="css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet">

	<?if(!Yii::app()->user->isGuest):?>
    	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
	<?endif;?>
    <script type="text/javascript" src="js/jquery.selectbox.min.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.js"></script>
    <script type="text/javascript" src="js/bootstrap-modal.js"></script>
    <script type="text/javascript" src="js/bootstrap-modalmanager.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <script src="//api-maps.yandex.ru/2.0.31/?load=package.standard,package.geoObjects,package.regions&lang=ru-RU" type="text/javascript"></script> 
    <script type="text/javascript" src="js/map.js"></script>
    <!--[if IE 8]>
      <link rel="stylesheet" type="text/css" href="css/ie8.css">
    <![endif]-->
</head>
<body>
<div id="wrapper">

    <div id="header">
        <div class="bottom-line"></div>
        <div class="row-fluid">
            <div class="span3">
                <div class="logo"><a href="<?php echo url('/'); ?>"><img src="img/elem/logo.png" alt=""></a></div>
            </div>
            <div class="span9">
                <div class="main-navigation logged-in">
                    <ul>
                        <li class="my-note <?if(!Yii::app()->user->isGuest):?>current<?endif;?>" data-panel="my-note-panel"><a href="#">Мой блокнот</a></li>
                        <li class="filter" data-panel="filter-panel"><a href="#">Фильтры</a></li>
                        <li class="search" data-panel="search-panel"><a href="#">Поиск</a></li>
                        <li class="news" data-panel="news-panel"><a href="#">Уведомления<div class="news-numder">3</div></a></li>
                    </ul>
                </div>

                <div class="control-panel logged-in">
                    <ul>
                        <?php if(Yii::app()->user->isGuest) : ?>
                       		<li class="enter-button current" data-panel="login-panel"><a href="#">Войти</a></li>
                        <?php endif; ?>
                        <li class="location" data-panel="locatio-panel"><a href="#"><?= Yii::app()->session['city']?></a></li> 
                        <li class="navigation-add-button"><a href="<?php echo url('entry/create'); ?>">Добавить</a></li>
                    </ul>
                </div>
            </div>
        </div>
		
	</div><!-- #header-->
	
	<div id="content">
        <!-- Panels -->
           <div class="notePanels"> <!-- My Note Pannel -->
                   <div class="fullwidth my-note-panel" style="display:none;">
                       <div class="leftSideBar">
                         <ul class="innerMenu">
                           <li><a href="#"><img src="img/elem/icon1.png" alt=""/>Папки</a></li>
                           <li><a href="#"><img src="img/elem/icon2.png" alt=""/>Записи</a></li>
                           <li><a href="#"><img src="img/elem/icon3.png" alt=""/>Сообщения <span class="colorG">+18</span></a></li>
                           <li><a href="#"><img src="img/elem/icon4.png" alt=""/>Места</a></li>
                           <li><a href="#"><img src="img/elem/icon5.png" alt=""/>Группы</a></li>
                           <li><a href="#"><img src="img/elem/icon6.png" alt=""/>Контакты</a></li>
                         </ul>
                       </div>
                       <div class="rightSideBar">
                         <ul class="innerMenu rightSideMenu">
                           <li><a href="#"><img src="img/elem/icon7.png" alt=""/>Настройки</a></li>
                           <li><a href="<?= url('site/logout')?>"><img src="img/elem/icon8.png" alt=""/>Выход</a></li>
                         </ul>
                       </div>
                   </div>
                   <!-- My Note Pannel END-->

                   <!-- Search Pannel -->
                   <div class="fullwidth search-panel" style="display:none;">
                       <div class="row-fluid">
                           <div class="span12">
                               <div class="leftSideBar">
                                   <div class="checkbox-holder">
                                       <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                       <label>Учитываются активные фильтры</label>
                                   </div>
                               </div>
                               <div class="rightSideBar groupedRadio">
                                   <div class="radio-holder">
                                       <span><input type="radio" value="" name="" class="radio"/></span>
                                       <label>По всему сервису</label>
                                   </div>
                                   <div class="radio-holder">
                                       <span><input type="radio" value="" name="" class="radio"/></span>
                                       <label>По моим записям</label>
                                   </div>

                               </div>
                           </div>
                       </div>
                       <div class="row-fluid">
                           <div class="span12 form-row ">
                               <div class="buttonHolder right"><a href="#" class="blueButton">Искать</a></div>
                               <div style="margin-right:83px;overflow:hidden;"><input type="text" placeholder="Начните ввод запроса в поисковую строку" class="styled-input">
                               <span class="mice-ico"></span></div>
                           </div>

                       </div>
                   </div>
                   <!-- Search Pannel END-->

                   <!-- Login Pannel -->
				   <?php if(Yii::app()->user->isGuest) : ?>
                   <div class="fullwidth login-panel" style="display:none;">
                       <div class="row-fluid">
                           <div class="span4">
                               <div class="innerSpan">
                                   <? $this->widget('UserLogin',array());?>
                               </div>
                           </div>
                           <div class="span4">
                               <div class="innerSpan">
                                   <? $this->widget('UserRegister',array());?>
                               </div>
                           </div>
                           <div class="span4">
                               <h5 class="bigMargin">Какие преимущества залогиненного?</h5>
                               <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
                           </div>
                       </div>

                   </div>
				   <?endif;?>
                   <!-- Login Pannel END-->

                   <!-- locatio Pannel -->
                   <div class="fullwidth locatio-panel" style="display:none;">
                       <div class="row-fluid">
                           <div class="span4">
                             <h5>Выберите регион</h5>
                           </div>
                           <div class="span8 edPan6">
                             <h5>Уточните ваше местоположение</h5>
                           </div>
                       </div>
                       <div class="row-fluid" style="margin-bottom:12px;">
                           <div class="span4">
							 <?php
							 $regions = Region::model()->findAll(array('order' => 'title'));
							 $region_options = array();
							 foreach($regions as $region)
							 {
								 $region_options[(string)$region->title] = $region->title;
							 }
							 ?>
                             <?= CHtml::dropDownList('region', NULL, $region_options, array('id' => 'region'));?>
                           </div>
                           <div class="span8 edPan6">
                               <div class="buttonHolder right"><a href="#" class="blueButton" id="detect-auto" onclick="addPlacemark(); return false;">Определить автоматически</a></div>
                               <div style="margin-right:200px;overflow:hidden;"><input type="text" class="customInput styled-input" name="text" placeholder="Начните вводить название улицы..." id="address"/></div>
                           </div>
                       </div>

                       <div class="row-fluid mapHolder">
                           <i>Вы можете воспользоваться полями сверху, либо прямо на карте передвинуть точку в нужное место</i>
                           <!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (начало) -->
                           <div id="map" style="width: 100%; height: 235px;"></div>
                           <!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->
                       </div>

                       <div class="row-fluid pointHolder">
                           <div class="buttonHolder right"><a href="#" class="blueButton">Сохранить</a></div>
                           <div class="mapInput">
                             <input class="customInput" type="text" name="href" placeholder="Например, Дом, Работа, Дача..."/>
                           </div>
                           <div class="fr textSide">
                             <p style="color:#272727;">Сохранить эту точку как (Это будет видно только вам)</p>
                           </div>
                       </div>
                   </div>
                   <!-- locatio Pannel END-->

                   <!-- News Pannel -->
                   <div class="fullwidth news-panel" style="display:none;">
                       <div class="row-fluid">
                           <div class="span4 newsPanelBlock">
                               <div class="newsPanelHeading clearfix">
                                   <h5>Уведомления сервиса <span class="number"> +5 новых</span></h5>
                                   <a href="#" class="showAll">Смотреть все</a>
                               </div>
                               <ul>
                                   <li>На вас подписался <a href="#">Венглинский Д.</a> <span class="date">18.04.2013 16:45</span></li>
                                   <li><a href="#">Венглинский Д.</a> прислал вам <a href="#">личное сообщение</a> <span class="date">18.04.2013 16:45</span></li>
                                   <li class="stonged">У вас заканчивается подписка на что-то <a href="#">Продлить</a></li>
                                   <li><a href="#">Васильев Д.</a> пожаловался на вашу запись <a href="#">Название записи</a> <span class="date">18.04.2013 16:45</span></li>
                                   <li>На вас подписался <a href="#">Венглинский Д.</a> <span class="date">18.04.2013 16:45</span></li>
                                   <li><a href="#">Венглинский Д.</a> прислал вам <a href="#">личное сообщение</a> <span class="date">18.04.2013 16:45</span></li>
                                   <li>У вас заканчивается подписка на что-то <a href="#">Продлить</a></li>
                                   <li><a href="#">Васильев Д.</a> пожаловался на вашу запись <a href="#">Название записи</a> <span class="date">18.04.2013 16:45</span></li>
                               </ul>
                           </div>
                           <div class="span4 newsPanelBlock">
                               <div class="newsPanelHeading clearfix">
                                   <h5>Комментарии <span class="number"> +167 новых</span></h5>
                                   <a href="#" class="showAll">Смотреть все</a>
                               </div>
                               <ul>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> оставил комментарий в вашей записи <a href="#">«Название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Орлов Ю.</a> оставил <a href="#">ответ</a> на ваш <a href="#">комментарий</a> в записи <a href="#">«Название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Орлов Ю.</a> оставил <a href="#">ответ</a> на <a href="#">комментарий</a> на который вы подписаны в записи <a href="#">«Название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> оставил комментарий в вашей записи <a href="#">«Название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Орлов Ю.</a> оставил <a href="#">ответ</a> на ваш <a href="#">комментарий</a> в записи <a href="#">«Название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Орлов Ю.</a> оставил <a href="#">ответ</a> на <a href="#">комментарий</a> на который вы подписаны в записи <a href="#">«Название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                               </ul>
                           </div>
                           <div class="span4 newsPanelBlock">
                               <div class="newsPanelHeading clearfix">
                                   <h5>Записи <span class="number"> +7 новых</span></h5>

                                   <a href="#" class="showAll">Смотреть все</a>
                               </div>

                               <ul>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Очень-очень длинное название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Noname»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Просто название»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Тут тоже ничего особенного»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Очень-очень длинное название записи»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Noname»</a> <span class="date">17.03.2012 09:56</span></li>
                                   <li class="stonged"><a href="#">Венглинский Д.</a> создал новую запись <a href="#">«Просто название»</a> <span class="date">17.03.2012 09:56</span></li>
                               </ul>
                           </div>
                       </div>
                       <div class="bottomGradient"></div>
                   </div>
                   <!-- News Pannel END-->

                   <!-- filter Pannel -->
                   <div class="fullwidth filter-panel menuHolderRibbon" style="display:none;">
                       <div class="row-fluid">
                           <div class="span2">
                               <p class="label">Доступ к записи <a href="#" class="reset">Сброс</a></p>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Мои записи</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Приватные</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Публичные</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Любой</label>
                               </div>
                           </div>
                           <div class="span2">
                               <p class="label">Тип записи <a href="#" class="reset">Любой</a></p>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Записи</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Отзывы</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Предсказания</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Сны</label>
                               </div>
                           </div>
                           <div class="span2">
                               <p class="label">Содержание <a href="#" class="reset">Любое</a></p>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>С фотографией</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>С аудиозаписью</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>С видеозаписью</label>
                               </div>
                               <div class="rightSideFormCrad checkbox-holder">
                                 <span><input class="checkBox" type="checkbox" name="visibleall"></span>
                                 <label>Любое содержание</label>
                               </div>
                           </div>
                           <div class="span3">
                               <p class="label">За период <a href="#" class="reset m-r">Сброс</a></p>
                               <select>
                                 <option>Весь период</option>
                               </select>
                               <p class="label" style="margin-top: 7px;">В регионе <a href="#" class="reset m-r">Сброс</a></p>
                               <select>
                                 <option>Все регионы</option>
                               </select>
                           </div>
                           <div class="span3 gridRibbon2">
                               <p class="label">Популярность</p>
                               <ul class="littleForm groupedRadio">
                                 <li>
                                   <div class="radio-holder">
                                       <span><input type="radio" value="r1" name="radio1" class="radio"/></span>
                                       <label>Самые свежие</label>
                                   </div>
                                 </li>
                                 <li>
                                   <div class="radio-holder">
                                       <span><input type="radio" value="r2" name="radio2" class="radio"/></span>
                                       <label>Самые рейтинговые</label>
                                   </div>
                                 </li>
                                 <li>
                                   <div class="radio-holder">
                                       <span><input type="radio" value="r3" name="radio3" class="radio"/></span>
                                       <label>Самые популярные</label>
                                   </div>
                                 </li>
                                 <li>
                                   <div class="radio-holder">
                                       <span><input type="radio" value="r4" name="radio4" class="radio"/></span>
                                       <label>Самые обсуждаемые</label>
                                   </div>
                                 </li>
                               </ul>
                           </div>
                       </div>
                       <div class="row-fluid">
                           <div class="span12 form-row ">
                               <a href="#" class="resetFliters">Сбросить все фильтры</a>
                               <div class="buttonHolder"><a href="#" class="blueButton">Искать</a></div>
                               <div style="margin-right: 235px; position: relative;"><input type="text" placeholder="Для дополнительной фильтрации вы можете ввести прямо тут поисковый запрос" class="styled-input">
                               <span class="mice-ico"></span></div>
                           </div>
                       </div>
                   </div>
                   <!-- filter Pannel END-->
            </div>
        <!-- Panels END -->
		<? echo $content; ?>
	</div>
</div> <!-- #wrapper -->


<div id="footer">
    <div class="row-fluid">
        <div class="span3">
            <a href="#" class="bottom-logo"><img src="img/elem/logo.png" alt=""></a>
            <ul class="social-buttons">
                <li><a href="#"><img src="img/icons/facebook-ico.png" alt=""></a></li>
                <li><a href="#"><img src="img/icons/google-ico.png" alt=""></a></li>
                <li><a href="#"><img src="img/icons/link-ico.png" alt=""></a></li>
                <li><a href="#"><img src="img/icons/mail-co.png" alt=""></a></li>
                <li><a href="#"><img src="img/icons/ok-ico.png" alt=""></a></li>
                <li><a href="#"><img src="img/icons/vk-ico.png" alt=""></a></li>
                <li><a href="#"><img src="img/icons/yandex-ico.png" alt=""></a></li>
            </ul>
            <p class="copyright">
                © 2013 Все права защищены
            </p>
        </div>
        <div class="span3">
            <ul class="bottom-nav">
                <li class="main-link"><a href="#">О сервисе</a></li>
                <li><a href="#">О Блокноте</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Условия договора</a></li>
                <li><a href="#">Политика конфиденциальности</a></li>
                <li><a href="<?php echo url('feedback'); ?>">Разработчики сервиса</a></li>
            </ul>
        </div>
        <div class="span3">
            <ul class="bottom-nav">
                <li class="main-link"><a href="#">Блокнот пользователя</a></li>
                <li><a href="<?php echo url('entry'); ?>">Записи</a></li>
                <li><a href="<?php echo url('message'); ?>">Сообщения</a><span class="number">+18</span></li>
                <li><a href="<?php echo url('place'); ?>">Места</a></li>
                <li><a href="<?php echo url('settings'); ?>">Настройки</a></li>
                <li><a href="<?php echo url('site/logout'); ?>">Выход</a></li>
            </ul>
        </div>
        <div class="span3">
            <a href="#" class="to-top-link">Поднять</a>
            <p>Использовано дискового пространства:<br> Mb из 100 Mb (93%)</p>
        </div>
    </div>
</div><!-- #footer -->

</body>