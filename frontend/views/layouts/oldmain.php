<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/examples.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.tagit.css"/>

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/autoclear.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/tag-it.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/typeahead.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/main.js'); ?>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <script type="text/javascript">
        $(function () {

        })

    </script>
    <style>

    </style>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
        <div id="mainmenu">
            <?php $this->widget(
                'zii.widgets.CMenu',
                array(
                    'items'=>array(
                        array(
                            'label'=>'Мой блокнот',
                            'url'=>'#',
                            'linkOptions'=>array(
                                'id'=>'menu_item_bloknot'
                            ),
                        ),
                        array(
                            'label'=>'Фильтры',
                            'url'=>'#',
                            'linkOptions'=>array(
                                'id'=>'menu_item_filter'
                            ),
                        ),
                        array(
                            'label'=>'Поиск',
                            'url'=>'#',
                            'linkOptions'=>array(
                                'id'=>'menu_item_search'
                            ),
                        ),
                        array(
                            'label'=>'Уведомления',
                            'url'=>'#',
                            'linkOptions'=>array(
                                'id'=>'menu_item_notification'
                            ),
                        ),
                    ),
                    'htmlOptions'=>array('style'=>'float:left;'),
                )
            ); ?>
            <?php $this->widget(
                'zii.widgets.CMenu',
                array(
                    'items'=>array(
                        array(
                            'label'=>'Санкт-Петербург',
                            'url'=>'#',
                            'linkOptions'=>array(
                                'id'=>'menu_item_geolocate'
                            ),
                        ),
                        array(
                            'label'=>'Войти',
                            'url'=>"#",
                            'visible'=>Yii::app()->user->isGuest,
                            'linkOptions'=>array('id'=>'menu_item_login')
                        ),
                        array(
                            'label'=>'Добавить',
                            'url'=>Yii::app()->createUrl('entry/create')
                        ),
                    ),
                    'htmlOptions'=>array('style'=>'float:right;')
                )
            ); ?>
        </div>
    </div>
    <!-- mainmenu -->

    <div id="block_bloknot_submenu" class="top_switchable">
        <?php $this->widget(
            'zii.widgets.CMenu',
            array(
                'items' => array(
                    array('label' => 'Папки', 'url' => array('/folder/index')),
                    array('label' => 'Записи', 'url' => array('/entry/index')),
                    array('label' => 'Сообщения', 'url' => array('/message/index')),
                    array('label' => 'Места', 'url' => array('/place/index')),
                    array('label' => 'Группы', 'url' => array('/group/index')),
                    array('label' => 'Контакты', 'url' => array('/contact/index'))
                ),
                'htmlOptions' => array('style' => 'float:left;')
            )
        ); ?>
        <?php $this->widget(
            'zii.widgets.CMenu',
            array(
                'items' => array(
                    array('label' => 'Настройки', 'url' => array('/entry/index')),
                    array(
                        'label' => 'Login',
                        'url' => array('/site/login'),
                        'visible' => Yii::app()->user->isGuest,
                    ),
                    array(
                        'label' => 'Logout (' . Yii::app()->user->name . ')',
                        'url' => array('/site/logout'),
                        'visible' => !Yii::app()->user->isGuest
                    )
                ),
                'htmlOptions' => array('style' => 'float:right;')
            )
        ); ?>
    </div>
    <div id="block_authority" class="top_switchable">
        <div style="float: left;">
            <?php $this->widget('InnerLoginForm'); ?>
        </div>
        <div>
            <?php $this->widget('InnerRegistrationForm'); ?>
        </div>
    </div>
    <div id="block_geolocation" class="top_switchable">
        <?php $this->widget('GeolocateOnMain'); ?>
    </div>
    <div id="block_filter" class="top_switchable">
        Filter
    </div>
    <div id="block_notification" class="top_switchable">
        Notification
    </div>
    <div id="block_search" class="top_switchable">
        Search
        <form method="get" action="<?php echo url('folder/index'); ?>">
            <input type="text" name="q" size="30" x-webkit-speech />
            <input type="submit" value="Google Search" />
        </form>
    </div>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
    </div>
    <!-- footer -->

</div>
<!-- page -->

</body>
</html>
