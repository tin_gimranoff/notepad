<div id="myModal" style="width: 610px" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>