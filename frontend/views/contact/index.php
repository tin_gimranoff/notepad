<div class="row-fluid wrapperGroupHolder">
    <div class="span12 headerGroup">
        <div class="groupLeft">
            <h1>Контакты</h1>
        </div>
        <div class="groupRight">
            <div class="buttonHolder">
                <?php echo CHtml::link('Добавить контакт', '#myModal', array('data-toggle'=>"modal",'class'=>"blueButton")); ?>
            </div>
            <div class="folderSearchHolder contactsSearch">
                <input class="styled-input" type="text" name="search" value="Поиск среди контактов"/>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid wrapperGroupContentHolder normal">
<div class="span12 simpleGroupHolder">
<div class="socialButtonsHolder clearfix">
    <span>Вы можете импортировать контакты из социальных сетей и почты</span>
    <ul class="socialButtonList">
        <li><a href="#"><img src="img/icons/icon-mail.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/mail-co.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/ok-ico.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/icon-twitter.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/vk-ico.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/yandex-ico.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/facebook-ico.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/google-ico.png" alt=""></a></li>
        <li><a href="#"><img src="img/icons/link-ico.png" alt=""></a></li>
    </ul>
    <a href="#" class="closeButton">Закрыть</a>
</div>
<div class="contactsListHolder clearfix">
    <?php if (sizeof($list)) : ?>
    <ul class="contactsList">
        <?php foreach ($list as $row) : ?>
        <li>
            <?php $this->renderPartial('_contact',array('model'=>$row)); ?>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>
</div>
</div>
<?php $this->renderPartial('create',array('model'=>$model)); ?>