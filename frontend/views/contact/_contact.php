<div class="cAvatar">
    <a href="#"><img src="img/elem/no-avatar.png" alt=""></a>
</div>
<?php if ($model->group_id) : ?>
<span class="cGroup">
    <a href="#"><?php echo Group::getGroupTitle($model->group_id); ?></a>
</span>
<?php else : ?>
<span class="cGroup">
    <a href="#" class="no-group">Без группы</a>
</span>
<?php endif; ?>
<span class="cName"><a href="#"><?php echo CHtml::encode($model->title); ?></a></span>
<!--<span class="cNumber">Записей в совместном доступе: 18</span>-->
<div class="cButton">
    <div class="buttonHolder">
        <a href="<?php echo Yii::app()->urlManager->createUrl('contact/update', array('id'=>$model->id)); ?>">Редактировать</a>
    </div>
</div>