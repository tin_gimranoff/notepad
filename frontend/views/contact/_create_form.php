<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'ActiveForm',
        array(
            'id' => 'contact-create-form',
            'enableAjaxValidation' => false,
        )
    ); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo CHtml::textField('person', 'Начните вводить фамилию, имя, e-mail контакта', array('size' => 60, 'maxlength' => 255, 'class' => 'autocomplete typeahead')); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255, 'value' => 'Как отображать этот контакт', 'class' => 'autoclear')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Добавить контакт'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->