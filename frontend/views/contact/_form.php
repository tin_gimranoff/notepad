<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */
?>
<style>
    .contact-type
    {

    }
    .contact-description
    {
        font-size: 10px;
    }
</style>
<script type="text/javascript">
    $(function() {
        $('#Contact_email').typeahead({
            name: 'person',
            remote: 'index.php?r=contact/complete&q=%QUERY',
            limit: 20,
            template: [
                '<p class="contact-name">{{value}}</p>',
                '<p class="contact-description">{{full}}</p>'
            ].join(''),
            engine: Hogan
        });
    })

</script>

<div class="form" style="padding-left: 30px;">

    <?php $form = $this->beginWidget(
        'ActiveForm',
        array(
            'id' => 'contact-create-form',
            'enableAjaxValidation' => false,
            'action'=>Yii::app()->urlManager->createUrl('contact/create')
        )
    ); ?>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->textField($model, 'email', array('value'=> 'Начните вводить фамилию, имя, e-mail контакта','size' => 60, 'maxlength' => 255, 'class' => 'styled-input typeahead')); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255, 'value' => 'Как отображать этот контакт', 'class' => 'styled-input')); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Добавить контакт'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->