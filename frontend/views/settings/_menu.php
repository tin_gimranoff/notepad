<div class="span3 left-side">
    <!--navigation-->
    <?php $this->widget(
        'zii.widgets.CMenu',
        array(
            'items'=>array(
                array(
                    'label'=>'Общая информация',
                    'url'=>array('/settings/index')
                ),
                array(
                    'label' => 'Смена пароля',
                    'url' => array('/settings/changePassword'),
                ),
                array(
                    'label' => 'Аватар аккаунта',
                    'url' => array('/settings/changeAvatar'),
                ),
                array(
                    'label' => 'Дисковое пространство',
                    'url' => array('/settings/spaceUsage'),
                ),
                array(
                    'label' => 'Баланс',
                    'url' => array('/settings/balance'),
                ),
                array(
                    'label' => 'Удаление аккаунта',
                    'url' => array('/settings/removeAccount'),
                ),
            ),
            'htmlOptions' => array('class'=>'menuNavigation'),
        )
    ); ?>
    <!--end:navigation-->
</div>