<div class="row-fluid">
    <?php $this->renderPartial('_menu', array()); ?>
    <div class="span9 main-content">
        <div class="headerContentSideBar">
            <h2>Текущий баланс: <span>1 912 рублей</span></h2>
            <div class="rightSideButtonHolder buttonHolder">
                <a href="#">Пополнить баланс</a>
            </div>
        </div>
        <div class="setupHolder clearfix">
            <p><strong>Услуга: <span>Увеличение дискового пространства на 100Mb</span><span> - до 20.05.2013</span></strong></p>
            <div class="gridButton clearfix">
                <div class="buttonHolder">
                    <a href="#">Добавить ещё 100 Mb (100 руб. в месяц)</a>
                </div>
                <div class="buttonHolder">
                    <a href="#">Продлить на месяц (100 руб.)</a>
                </div>
                <div class="buttonHolder">
                    <a href="#">Продлить на квартал (250 руб.)</a>
                </div>
            </div>
        </div>
        <div class="table-grid">
            <div class="headerBalanceHolder">
                <p>История баланса</p>
                <div class="innerDateHolder">
                    <label for="from">От</label>
                    <input type="text" id="from" name="from" />
                    <label for="to">До</label>
                    <input type="text" id="to" name="to" />
                </div>
            </div>
            <ul class="listGrid">
                <li>
                    <span class="panText panTotal plusTotal">+ 2 000 руб.</span>
                    <span class="panText panCenter">Пополнение счета</span>
                    <span class="panText panDate">22.05.2013 16:35</span>
                </li>
                <li>
                    <span class="panText panTotal plusTotal">+1 982 руб.</span>
                    <span class="panText panCenter">Пополнение счета</span>
                    <span class="panText panDate">11.03.2013 11:32</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 30 дней</span>
                    <span class="panText panDate">03.11.2012 03:18</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Красивый URL страницы»</span> на 30 дней</span>
                    <span class="panText panDate">22.05.2013 16:35</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 30 дней</span>
                    <span class="panText panDate">11.03.2013 11:32</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Красивый URL страницы»</span> на 30 дней</span>
                    <span class="panText panDate">03.11.2012 03:18</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 30 дней</span>
                    <span class="panText panDate">22.05.2013 16:35</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-1000 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 365 дней</span>
                    <span class="panText panDate">11.03.2013 11:32</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 30 дней</span>
                    <span class="panText panDate">03.11.2012 03:18</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 30 дней</span>
                    <span class="panText panDate">22.05.2013 16:35</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Красивый URL страницы»</span> на 30 дней</span>
                    <span class="panText panDate">11.03.2013 11:32</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-100 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Увеличение дискового пространства»</span> на 30 дней</span>
                    <span class="panText panDate">03.11.2012 03:18</span>
                </li>
                <li>
                    <span class="panText panTotal minusTotal">-1 000 руб.</span>
                    <span class="panText panCenter">Продление услуги <span>«Красивый URL страницы»</span> на 365 дней</span>
                    <span class="panText panDate">03.11.2012 03:18</span>
                </li>
            </ul>
        </div>
    </div>
</div>
