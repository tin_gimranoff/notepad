<div class="row-fluid">
    <?php $this->renderPartial('_menu', array()); ?>
    <div class="span9 main-content">
        <h3 class="settings-sub-header">
            Ваш текущий аватар
        </h3>
        <div class="settings-sub-holder clearfix">
            <div class="avatar-image size-222"><img src="img/content/avatar.jpg" alt=""></div>
            <div class="avatar-image size-100"><img src="img/content/avatar.jpg" alt=""></div>
            <div class="avatar-image size-50"><img src="img/content/avatar.jpg" alt=""></div>
            <div class="avatar-image size-20"><img src="img/content/avatar.jpg" alt=""></div>
            <div class="listDiskSpace-button"><a href="#">Удалить текущий аватар</a></div>
        </div>
        <h3 class="settings-sub-header">
            Загрузить новый аватар
        </h3>
        <div class="settings-sub-holder clearfix">
            <form action="#">
                <div class="form-row"><label for="url">Введите URL картинки</label><input type="text" name="url" id="url"></div>
                <div class="form-row">
                    <label for="load">Либо загрузите аватар с компьютера</label>
                    <div class="file-input">
                        <input type="file" class="file" name="load" id="load" />
                        <div class="fakefile">
                            <input type="text" class="filetext"/>
                            <div class="buttonHolder">
                                <a href="#">Обзор</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <input class="submitButton" type="submit" value="Сохранить" name="submit">
                </div>
            </form>
        </div>
        <h3 class="settings-sub-header">
            Выбрать аватар из нашей коллекции
        </h3>
        <div class="settings-sub-holder clearfix">
            <ul class="img-list">
                <li><a href="#"><img src="img/content/av-img1.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img2.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img3.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img4.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img5.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img6.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img7.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img8.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img9.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img10.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img11.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img12.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img13.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img14.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img15.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img16.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img17.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img18.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img19.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img20.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img21.jpg" alt=""></a></li>
                <li><a href="#"><img src="img/content/av-img22.jpg" alt=""></a></li>
            </ul>
            <div class="form-row">
                <input class="submitButton" type="submit" value="Выбрать" name="submit">
            </div>
        </div>
    </div>
</div>