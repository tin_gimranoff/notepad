<?php
/**
 * @var $model User
 * @var $this Controller
 */
?>
<div class="row-fluid">
    <?php $this->renderPartial('_menu', array()); ?>
    <div class="span9 main-content">
        <?php if (empty($model->alias)): ?>
        <div class="changeCrad">
            <p>У вас есть возможность сделать к своей странице красивое запоминающееся название, которое можно будет вводить прямо в строке браузера. Ваша текущая отображаемая ссылка:</p>
            <a class="borderBot" href="#">www.БЛОКНОТ.РФ/<span><?php echo $model->id; ?></span></a>
            <p>А может стать:</p>
            <a href="#">www.БЛОКНОТ.РФ/<span class="selectCrad"><?php echo $model->login; ?></span></a>
            <div class="buttonHolder">
                <a class="anButton" href="#">Подобрать красивое имя страницы</a>
            </div>
        </div>
        <?php endif; ?>
        <div class="wrapperFormCrad">
            <?php
            /**
             * @var $form ActiveForm
             */
            ?>
            <?php $form = $this->beginWidget('ActiveForm', array(
                    'id'=>"formCrad"
                ));?>
                <ul class="formCrad">
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'login'); ?>
                            <?php echo $form->textField($model,'login',array('class'=>'setup2 clearinput')); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'email'); ?>
                            <?php echo $form->textField($model,'email',array('class'=>'setup1 clearinput')); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'alter_email'); ?>
                            <?php echo $form->textField($model,'alter_email',array('class'=>'setup1 clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->alter_email_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'alter_email_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'name'); ?>
                            <?php echo $form->textField($model,'name',array('class'=>'setup2 clearinput')); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'surname'); ?>
                            <?php echo $form->textField($model,'surname',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->surname_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'surname_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'patronymic'); ?>
                            <?php echo $form->textField($model,'patronymic',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->patronymic_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'patronymic_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'skype'); ?>
                            <?php echo $form->textField($model,'skype',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->skype_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'skype_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'icq'); ?>
                            <?php echo $form->textField($model,'icq',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->icq_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'icq_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'phone1'); ?>
                            <?php echo $form->textField($model,'phone1',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->phone1_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'phone1_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'phone2'); ?>
                            <?php echo $form->textField($model,'phone2',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->phone2_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'phone2_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <div class="leftSideFormCrad">
                            <?php echo $form->label($model,'birthday'); ?>
                            <?php echo $form->textField($model,'birthday',array('class'=>'clearinput')); ?>
                        </div>
                        <div class="rightSideFormCrad <?php if ($model->birthday_v) echo "active"; ?>">
                            <span><?php echo $form->checkBox($model,'birthday_v',array('class'=>'checkBox')); ?></span>
                            <?php echo CHtml::label('Показывать всем', ''); ?>
                        </div>
                    </li>
                    <li>
                        <input class="submitButton" type="submit" value="Сохранить" name="submit"/>
                    </li>
                </ul>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
