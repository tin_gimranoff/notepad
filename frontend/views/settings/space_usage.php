<div class="row-fluid">
    <?php $this->renderPartial('_menu', array()); ?>
    <div class="span9 main-content">
        <div class="headerDiskSpace">
            <p>Использование дискового пространства: <span>93 Mb из 100 Mb</span></p>
        </div>
        <div class="loadDiskSpace">
            <div class="loadDiskSpaceGreen"></div>
            <div class="loadDiskSpaceRed"></div>
        </div>
        <div class="buttonDiskSpace">
            <div class="buttonHolder">
                <a href="#">Увеличить дисковое пространство на 100 Mb</a>
            </div>
        </div>
        <div class="listDiskSpace">
            <div class="headerListDiskSpace">
                <div class="listDiskSpace-name">
                    <p>Загруженные файлы (13)</p>
                </div>
                <div class="listDiskSpace-button"></div>
                <div class="listDiskSpace-size">
                    <a class="" href="#">Размер</a>
                </div>
                <div class="listDiskSpace-data">
                    <a class="sortBy" href="#">Дата</a>
                </div>
            </div>
            <ul class="listDiskSpaceInner">
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">картинка.jpg</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.34 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">sgsdfghdhdfh.rar</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>1.52 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">jnldfgjnen.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.01 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">image.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>12.98 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li><li>
                    <div class="listDiskSpace-name">
                        <a href="#">картинка.jpg</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.34 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">sgsdfghdhdfh.rar</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>1.52 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">jnldfgjnen.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.01 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">image.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>12.98 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li><li>
                    <div class="listDiskSpace-name">
                        <a href="#">картинка.jpg</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.34 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">sgsdfghdhdfh.rar</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>1.52 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">jnldfgjnen.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.01 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">image.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>12.98 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li><li>
                    <div class="listDiskSpace-name">
                        <a href="#">картинка.jpg</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.34 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">sgsdfghdhdfh.rar</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>1.52 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">jnldfgjnen.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>0.01 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
                <li>
                    <div class="listDiskSpace-name">
                        <a href="#">image.png</a>
                    </div>
                    <div class="listDiskSpace-button">
                        <a href="#">Удалить</a>
                    </div>
                    <div class="listDiskSpace-size">
                        <p>12.98 Mb</p>
                    </div>
                    <div class="listDiskSpace-data">
                        <p>24.05.2013</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
