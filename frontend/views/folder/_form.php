<?php $form = $this->beginWidget(
    'ActiveForm',
    array(
        'id' => 'folder-form',
        'enableClientValidation' => false,
        'action' => ($model->isNewRecord)?array('folder/create'):array('folder/update', 'id'=>$model->id),
    )
);

echo $form->errorSummary($model);

echo $form->textField($model, 'title', array('size'=>35,'value'=>($model->title)?$model->title:'Введите название папки','class'=>'styled-input'));
echo $form->error($model, 'title');

echo CHtml::submitButton(($model->isNewRecord)?'Сохранить папку':'Сохранить изменения');

$this->endWidget();