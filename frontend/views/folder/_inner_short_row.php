<?php
/**
 * @var $folderRow array
 */
?>
<div class="groupListBlock clearfix">
    <div class="logoGroup">
        <div class="headerImgHolderGroup">
            <img src="img/content/folderImg.png" alt=""/>
        </div>
        <div class="button-holder">
            <a href="<?php echo Yii::app()->urlManager->createUrl('entry/create',array('folder'=>$folderRow['id'])); ?>" class="green-button">Добавить запись</a>
        </div>
    </div>
    <div class="headrGroup">
        <h1><?php echo $folderRow['title']; ?> (6 записей)</h1>
        <div class="buttonHolder">
			<?php echo CHtml::link('Редактировать',url('folder/update'),array('data-toggle'=>"modal",'data-upload'=>'ajax','object_id'=>$folderRow['id'])); ?>
        </div>
    </div>
    <ul class="innerListGroup sortable">
        <li>
            <img src="img/elem/drag.png" class="handlerIco" alt="">
            <p><a class="iconSmile" href="#">This is Photoshop's version  of Lorem Ipsum </a><a class="commentsFolderHolder" href="#">+85</a></p>
            <div class="listFolderHolderText">
                <p>Сегодня в 16:56</p>
            </div>
        </li>
    </ul>
</div>