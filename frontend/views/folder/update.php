<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	<?php echo CHtml::link('Удалить',Yii::app()->urlManager->createUrl('folder/delete', array('id'=>$model->id)),array('class'=>'delete_object')); ?>
</div>
<div class="modal-body">
	<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>