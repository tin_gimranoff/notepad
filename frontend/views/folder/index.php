<?php
/**
 * @var $this folderController
 * @var $model Folder
 * @var $list array
 * TODO Saving changed tab to cookie
 */

clientScript()->registerScript('folder_check', '
$(function() {

    $(".folder").on("click", function() {

        var folder = $(this).attr("folder");
        $(".folder_entries_header").html("<h2>" + $(this).attr("title") + "</h2>");

        $.ajax({
            url: "index.php?r=folder/entries",
            data: {folder: folder},
            dataType: "json",
            type: "POST"
        }).done(function(data) {

            var out = "";
            for (var i=0; i < data.length; i++) {
                var theDate = new Date(data[i].date_created*1000);
                dateString = theDate.toLocaleString();

                out+=
                    "<div class=\"folder_entries_item\">" +
                    "<a href=\"' . url('entry/view') . '&id=" + data[i].id + "\">" + data[i].title + "</a>" +
                     "<span class=\"date\">" + dateString + "</span>" +
                    "</div>"
            }
            $(".folder_entries_content").html(out);

            $(".folder_entries_content").sortable();
        });
    });
})
');
?>

<div class="row-fluid wrapperGroupHolder">
    <div class="span12 headerGroup">
        <div class="groupLeft">
            <h1>Папки</h1>
            <ul class="wrapperGroup">
                <li class="expansionGroup active"><a href="#">Упрощенный вид</a></li>
                <li class="simpleGroup"><a href="#">Расширенный вид</a></li>
            </ul>
        </div>
        <div class="groupRight">
            <div class="buttonHolder">
				<?php echo CHtml::link('Добавить папку', '#myModal', array('data-toggle'=>"modal",'class'=>'blueButton')); ?>
            </div>
            <div class="folderSearchHolder">
                <input class="styled-input" type="text" name="search" value="Поиск по названию или содержанию"/>
            </div>
        </div>
    </div>
</div>

<!--Normal container for folders-->
<div class="row-fluid wrapperGroupContentHolder normal">
    <div class="span12 simpleGroupHolder">
        <?php if (sizeof($folderList)) : ?>
            <?php foreach ($folderList as $folderRow) : ?>
                <?php $this->renderPartial('_inner_short_row', array('folderRow'=>$folderRow)); ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>

<div class="row-fluid wrapperGroupContentHolder extended tabs">
    <div class="span3 left-side">
        <ul class="navGroupList">
            <?php if (sizeof($folderList)) : ?>
                <?php foreach ($folderList as $key=>$folderRow) : ?>
                    <li><a href="#<?php echo "tab".($key+1); ?>"><span class="borderSpan"></span><?php echo $folderRow['title']; ?><span class="activeBorder"></span></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <div class="addButton">
			<?php echo CHtml::link('Добавить папку', '#myModal', array('data-toggle'=>"modal")); ?>
        </div>
    </div>
    <div class="span9 main-content clearfix">
        <?php if (sizeof($folderList)) : ?>
            <?php foreach ($folderList as $key=>$folderRow) : ?>
                <div class="groupListBlock clearfix" id="<?php echo "tab" . ($key+1); ?>">
                <?php if ($key==0) : ?>
                    <?php echo $this->renderPartial('_inner_extended_row', array('folderRow'=>$folderRow,'tab'=>2)); ?>
                <?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
<?php $this->renderPartial('create', array('model'=>$folder)); ?>