<?php
/**
 * @var $entry Entry
 * @var $form ActiveForm
 */
?>
<div id="block_review_restaurant" class="review_block_inner">
    <table>
        <?php foreach (array(
                           'r_ratio',
                           'r_comfort',
                           'r_kitchen',
                           'r_assortment',
                           'r_situation',
                           'r_service'
                       ) as $attribute) : ?>
            <?php $this->renderPartial('_inner_review_field', array(
                    'model'=>$entry,
                    'attribute'=>$attribute,
                    'form'=>$form
                )); ?>
        <?php endforeach; ?>
    </table>
</div>