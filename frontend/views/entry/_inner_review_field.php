<?php
/**
 * @var string $attribute
 * @var $model Review
 */
?>
<tr>
    <td><?php echo $model->getAttributeLabel($attribute); ?></td>
    <td>
        <?php echo CHtml::label(Entry::getScoreList(Entry::REVIEW_SCORE_TERRIBLE), ''); ?>
        <?php echo $form->radioButton($model, $attribute); ?>
    </td>
    <td>
        <?php echo CHtml::label(Entry::getScoreList(Entry::REVIEW_SCORE_BAD), ''); ?>
        <?php echo $form->radioButton($model, $attribute); ?>
    </td>
    <td>
        <?php echo CHtml::label(Entry::getScoreList(Entry::REVIEW_SCORE_NORMAL), ''); ?>
        <?php echo $form->radioButton($model, $attribute); ?>
    </td>
    <td>
        <?php echo CHtml::label(Entry::getScoreList(Entry::REVIEW_SCORE_GOOD), ''); ?>
        <?php echo $form->radioButton($model, $attribute); ?>
    </td>
    <td>
        <?php echo CHtml::label(Entry::getScoreList(Entry::REVIEW_SCORE_EXCELLENT), ''); ?>
        <?php echo $form->radioButton($model, $attribute); ?>
    </td>
</tr>