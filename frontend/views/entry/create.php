<h1>Добавление записи</h1>
<div class="row-fluid wrapperGroupContentHolder">
    <?php echo $this->renderPartial('_form', array(
            'entry' => $entry,
            'author' => $author,
            'place' => $place,
            'folder'=>$folder
        )
    ); ?>
</div>