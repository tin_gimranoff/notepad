<?php
/**
 * @var $review Review
 * @var $form ActiveForm
 */
?>
<div id="block_review_car" class="review_block_inner">
    <table>
        <?php foreach (array(
                           'r_ratio',
                           'r_comfort',
                           'r_safety',
                           'r_economy',
                           'r_features',
                           'r_appearance'
                       ) as $attribute) : ?>
            <?php $this->renderPartial('_inner_review_field', array(
                    'model'=>$entry,
                    'attribute'=>$attribute,
                    'form'=>$form
                )); ?>
        <?php endforeach; ?>
    </table>
</div>