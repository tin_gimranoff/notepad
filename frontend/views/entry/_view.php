<?php
/* @var $this EntryController */
/* @var $data Entry */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
    <span object_id="<?php echo $data->id; ?>" class="with-suscribe not-subscribed"></span>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('author')); ?>:</b>
	<?php echo Author::getNameById($data->author); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rating')); ?>:</b>
	<?php echo CHtml::encode($data->rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo Entry::getTypeList($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('privacy')); ?>:</b>
	<?php echo CHtml::encode($data->privacy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<?php switch ($data->type) :
		case Entry::ENTRY_TYPE_NORMAL:
			break;
		case Entry::ENTRY_TYPE_DREAM:
			?>
			<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
			<?php echo CHtml::encode($data->date_created); ?>
			<br /> <?php
			break;
		case Entry::ENTRY_TYPE_PREDICTION:
			break;
		case Entry::ENTRY_TYPE_REVIEW:
			break;
	endswitch; ?>
</div>