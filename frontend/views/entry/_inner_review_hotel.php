<?php
/**
 * @var $entry Entry
 * @var $form ActiveForm
 */
?>
<div id="block_review_hotel" class="review_block_inner">
    <table>
        <?php foreach (array(
                           'r_ratio',
                           'r_location',
                           'r_purity',
                           'r_comfort',
                           'r_services',
                           'r_personnel'
                       ) as $attribute) : ?>
            <?php $this->renderPartial('_inner_review_field', array(
                    'model'=>$entry,
                    'attribute'=>$attribute,
                    'form'=>$form
                )); ?>
        <?php endforeach; ?>
    </table>
</div>