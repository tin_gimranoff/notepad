<?php
Yii::app()->clientScript->registerScript(
    'update-entry-listview','

    $(".entry_type_item").click(function() {

        $(".entry_type_elem").removeClass("active");
        $(this).parent(".entry_type_elem").addClass("active");
        var type = $(this).attr("type");

        $.fn.yiiListView.update(
            "entry-list", {
                data: {"type":type},
                url:"index.php?r=entry/index",
            }
        )
   });
   ', CClientScript::POS_END);
?>
<div class="row-fluid wrapperGroupHolder">
    <div class="span12 headerGroup">
        <div class="groupLeft">
            <h1>Записи</h1>
            <ul class="wrapperGroup">
                <li class="entry_type_elem active"><a class="entry_type_item" type="" href="#">Все записи</a></li>
                <li class="entry_type_elem"><a class="entry_type_item" type="<?php echo Entry::ENTRY_TYPE_NORMAL; ?>" href="#">Записи</a></li>
                <li class="entry_type_elem"><a class="entry_type_item" type="<?php echo Entry::ENTRY_TYPE_DREAM; ?>" href="#">Сны</a></li>
                <li class="entry_type_elem"><a class="entry_type_item" type="<?php echo Entry::ENTRY_TYPE_PREDICTION; ?>" href="#">Предсказания</a>
                </li>
                <li class="entry_type_elem"><a class="entry_type_item" type="<?php echo Entry::ENTRY_TYPE_REVIEW; ?>" href="#">Отзывы</a></li>
            </ul>
        </div>
        <div class="groupRight">
            <ul class="wrapperGroup">
                <li class="expansionGroup active"><a href="#">Списком</a></li>
                <li class="listPlaces"><a href="#">На карте</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="row-fluid">
    <?php $this->widget(
        'zii.widgets.CListView',
        array(
            'id' => 'entry-list',
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
        )
    ); ?>
</div>