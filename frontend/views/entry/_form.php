<?php
/**
 *
 * @var $this EntryController
 * @var $entry Entry
 * @var $form ActiveForm
 * @var $author Author
 * @var $review Review
 * @var $dream Dream
 * @var $prediction Prediction
 */

Yii::app()->clientScript->registerCss('privacy', '
#block_privacy_list
{
    display: none;
}
#block_review
{
    display: none;
}
#block_prediction
{
    display: none;
}
#block_dream
{
    display: none;
}
');

Yii::app()->clientScript->registerScript('privacy', '
$(function() {
    $("#Entry_privacy").on("change", function(e) {
        var privacy = $(this).val();
        if (privacy == "alternates") {
            $("#block_privacy_list").show();
        }
        else {
            $("#block_privacy_list").hide();
        }
    })

    $("#Entry_type").on("change", function(e) {

        $(".collapsible-block").hide();

        var type = parseInt($(this).val());

        switch (type) {

            case entry_type_normal:
                $("h1").html("Добавление записи");
                $("input[id=entry_submit]").val("Добавить записи");
                break;

            case entry_type_dream:
                $("h1").html("Добавление сна");
                $("input[id=entry_submit]").val("Добавить сон");
                $("#block_dream").show();
                break;

            case entry_type_prediction:
                $("h1").html("Добавление предсказания");
                $("input[id=entry_submit]").val("Добавить предсказание");
                $("#block_prediction").show();
                break;

            case entry_type_review:
                $("h1").html("Добавление отзыва");
                $("input[id=entry_submit]").val("Добавить отзыв");
                $("#block_review").show();
                break;
        }
    }); //

    $("#agreement").on("click", function() {
        if ($(this).is(":checked")) {
            $("#entry_submit").removeClass("disabled");
            $("#entry_submit").removeAttr("disabled");
        }
        else {
            $("#entry_submit").addClass("disabled");
            $("#entry_submit").attr("disabled", "disabled");
        }
    }); // event on click #agreement

    $("#Review_subject").on("change", function() {

        console.log($(this).val());
        switch ($(this).val()) {

            case "' . Entry::REVIEW_TYPE_CAR . '":
                $(".review_block_inner").hide();
                $("#block_review_car").show();
                break;

            case "' . Entry::REVIEW_TYPE_HOTEL . '":
                $(".review_block_inner").hide();
                $("#block_review_hotel").show();
                break;

            case "' . Entry::REVIEW_TYPE_RESTAURANT . '":
                $(".review_block_inner").hide();
                $("#block_review_restaurant").show();
                break;

            default: break;
        }
    });
})
', CClientScript::POS_END);
?>

	<?php $form = $this->beginWidget('ActiveForm', array(
			'id'=>'entry-form',
			'enableAjaxValidation'=>false,
		)); ?>

	<?php echo $form->errorSummary($entry); ?>

	<div class="row">
        <div style="float: left;">
            <?php echo $form->labelEx($entry,'title'); ?>
            <?php echo $form->textField($entry,'title',array('size'=>40,'maxlength'=>200)); ?>
            <?php echo $form->error($entry,'title'); ?>
        </div>
        <div style="float: left">
            <?php echo $form->labelEx($entry,'type'); ?>
            <?php echo $form->dropDownList($entry, 'type', Entry::getTypeList(), array('options' => array('normal' => array('selected' => 'selected')))); ?>
            <?php echo $form->error($entry,'type'); ?>
        </div>
        <div>
            <?php echo $form->labelEx($entry,'privacy'); ?>
            <?php echo $form->dropDownList($entry,'privacy', Entry::getPrivacyList()); ?>
            <?php echo $form->error($entry,'privacy'); ?>
        </div>
	</div>

	<div class="row">
		<?php echo $form->textArea($entry,'text',array('rows'=>6, 'cols'=>120)); ?>
		<?php echo $form->error($entry,'text'); ?>
	</div>

	<div id="block_author" class="row">
		<?php echo $form->labelEx($entry,'author'); ?>

		<?php echo $form->textField($author, 'email', array('value' => 'E-mail', 'class' => "autoclear")); ?>
        <?php echo $form->textField($author, 'firstname', array('value' => 'Имя', 'class' => "autoclear")); ?>
        <?php echo $form->textField($author, 'lastname', array('value' => 'Фамилия', 'class' => "autoclear")); ?>
	</div>

	<div id="block_dream" class="collapsible-block">
		<div class="row">
			<?php echo $form->labelEx($entry, 'd_tags'); ?>
			<?php echo $form->textField($entry, 'd_tags', array('class' => 'tag-list')); ?>
		</div>
		<div class="row">
			<?php echo $form->dropDownList($entry, 'd_scale', Entry::getScaleList()); ?>
		</div>
	</div>

	<div id="block_prediction" class="row collapsible-block">
		<div class="row">
            <?php echo $form->labelEx($entry, 'p_destination'); ?>
            <?php echo $form->textField($entry, 'p_destination'); ?>
		</div>
	</div>

	<div id="block_review" class="collapsible-block">
		<div class="row">
            <h5>Данные об отзыве</h5>
            <?php echo $form->dropDownList($entry, 'r_subject', Entry::getReviewTypesList(), array('prompt'=>'О ком/чем отзыв')); ?>
            <?php echo $form->textField($entry, 'r_about', array('size' => 11, 'maxlength' => 11)); ?>
            <?php echo $form->dropDownList($entry, 'r_evaluation', Entry::getEvaluationList()); ?>
            <?php $this->renderPartial('_inner_review_car', array('entry'=>$entry,'form'=>$form)); ?>
            <?php $this->renderPartial('_inner_review_hotel', array('entry'=>$entry,'form'=>$form)); ?>
            <?php $this->renderPartial('_inner_review_restaurant', array('entry'=>$entry,'form'=>$form)); ?>
		</div>
	</div>

	<div id="block_privacy_list" class="row">
		<?php echo CHtml::label('Кому дать доступ к этой записи?', 'privacy_list'); ?>
		<?php echo $form->textField($entry,'privacy_list',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo CHtml::dropDownList('contacts', 'Выбрать из контактов', array(0 => 1)); ?>
	</div>

	<div id="block_placement">
        <?php echo CHtml::label('Место публикации', false); ?>
        <?php echo $form->dropDownList($place, 'region', array(), array('prompt'=>'Укажите регион')) ?>
        <?php echo $form->textField($place, 'address', array('value'=>'Адрес', 'class'=>'autoclear')); ?>
        <?php echo $form->textField($place, 'title', array('value'=>'Название точки', 'class'=>'autoclear')); ?>
        <?php echo CHtml::dropDownList('Place[place]', '', Place::getPlaceList(), array('prompt'=>'Выбрать из сохраненных')) ?>
	</div>

	<div class="row">
		<?php echo CHtml::link("Напомнить мне", array('recall/create'), array('class' => 'jModal')); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label("Я прочитал и согласен с <a href='" . Yii::app()->createUrl('site/agreement') . "'>правилами публикации записей</a> в системе БЛОКНОТ.РФ", 'agreement'); ?>
		<?php echo CHtml::checkBox('agreement', ''); ?>
	</div>

    <?php echo $form->hiddenField($entry, 'folder', array('value'=>$folder)); ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($entry->isNewRecord ? 'Добавить запись' : 'Сохранить изменения', array(
            'id' => 'entry_submit',
            'class'=>'disabled',
            'disabled'=>'disabled'
            )); ?>
	</div>

	<?php $this->endWidget(); ?>

