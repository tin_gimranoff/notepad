<?php

/**
 * Class LastReviews
 *
 * Widget for display last entries of type "Review".
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class LastReviews extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_reviews', array());
    }
}