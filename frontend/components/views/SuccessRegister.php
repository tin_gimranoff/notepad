<div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header clearfix">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	<h3 id="myModalLabel">Успешная регистрация</h3>
	<i>Спасибо за регистрацию.<br />На вашу почту выслано письмо для активации аккаунта</i>
  </div>
  <div class="modal-body clearfix">
  </div>
</div>

<script>
$(window).ready(function(){
	$('#myModal1').modal('show');
});
</script>