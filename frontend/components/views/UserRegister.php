<?php echo CHtml::beginForm(); ?>
<h5>Быстрая регистрация</h5>
<?if(count($model->getErrors()) > 0):?>
<ul style="margin-bottom: 5px;color: red;">
<?foreach($model->getErrors() as $error):?>
	<li><?=$error[0]?></li>
<?endforeach;?>
</ul>
<?endif;?>
<div class="form-row">
	<?php echo CHtml::activeTextField($model, 'email', array('class' => 'styled-input', 'placeholder' => 'Email')) ?>
</div>
<div class="form-row">
	<?php echo CHtml::activePasswordField($model, 'password', array('class' => 'styled-input', 'placeholder' => 'Пароль')) ?>
</div>
<div class="buttonHolder right">
	<?= CHtml::linkButton('Зарегистрироваться', array('class'=>'blueButton'));?>
</div>
<a href="#" class="form-link right">Полная форма</a>
<?php echo CHtml::endForm(); ?>