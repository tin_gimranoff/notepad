<?php echo CHtml::beginForm(); ?>
<h5>Войти в свой аккаунт</h5>
<?if(count($model->getErrors()) > 0):?>
<ul style="margin-bottom: 5px;color: red;">
<?foreach($model->getErrors() as $error):?>
	<li><?=$error[0]?></li>
<?endforeach;?>
</ul>
<?endif;?>
<div class="form-row">
	<?php echo CHtml::activeTextField($model, 'email', array('class' => 'styled-input', 'placeholder' => 'Email')) ?>
</div>
<div class="form-row">
	<?php echo CHtml::activePasswordField($model, 'password', array('class' => 'styled-input', 'placeholder' => 'Пароль')) ?>
</div>
<div class="buttonHolder right">
	<?= CHtml::linkButton('<span class="enterButton"></span>Войти', array('class'=>'blueButton'));?>
</div>
<a href="#myModal2" class="form-link right" role="button" data-toggle="modal">Забыли пароль?</a>
<?php echo CHtml::endForm(); ?>



			<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <?php echo CHtml::beginForm(); ?>
			  <div class="modal-header clearfix">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h3 id="myModalLabel">Забыли пароль?</h3>
				<i>Для восстановления пароля введите ваш Email.</i>
			  </div>
			  <div class="modal-body clearfix">
				<div class="span3">
                                      <?php echo CHtml::activeTextField($restore_model, 'login_or_email', array('placeholder' => 'Логин или Email', 'style' => 'width: 100%')) ?>
                                  </form> 
				</div>
				<div class="span2 customButtonGrid fl">
				 <!-- <input class="submitButton" type="submit" value="Отправить" name="submit"> -->
                                  <?= CHtml::submitButton('Отправить', array('class' => 'submitButton'))?>
				</div>
			  </div>
                            <?php echo CHtml::endForm(); ?>
			</div>