<?php

/**
 * Class LastNormalEntries
 *
 * Widget for display last normal entries.
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class LastNormalEntries extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_entries', array());
    }
}