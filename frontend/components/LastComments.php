<?php

/**
 * Class LastComments
 *
 * Widget for display last comments.
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class LastComments extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_comments', array());
    }
}