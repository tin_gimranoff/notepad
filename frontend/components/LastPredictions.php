<?php

/**
 * Class LastPredictions
 *
 * Widget for display last entries of type "Prediction".
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class LastPredictions extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_predictions', array());
    }
}