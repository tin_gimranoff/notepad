<?php

/**
 * Class InnerRegistrationForm
 *
 * Widget of registration form for embedding to layout.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class InnerRegistrationForm extends CWidget {

    public function init() {}

    public function run() {

        $registrationForm = new RegistrationForm();

        $this->render('application.views.site._inner_register_form', array(
                'model' => $registrationForm
            ));
    }
}