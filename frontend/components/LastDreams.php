<?php

/**
 * Class LastDreams
 *
 * Widget for display last entries of type 'Dream'.
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class LastDreams extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_dreams', array());
    }
}