<?php

/**
 * Class UserIdentity
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	public $email;

	const ERROR_EMAIL_INVALID = 3;
	const ERROR_STATUS_NOTACTIV = 4;
	const ERROR_STATUS_BAN = 5;

	public function __construct($email, $password)
	{
		$this->email = $email;
		$this->password = $password;
	}

	public function authenticate()
	{
		/**
		 * @var $user User
		 */
		$user = User::model()->findByAttributes(array('email' => $this->email));

		if ($user === null) {
			$this->errorCode = self::ERROR_EMAIL_INVALID;
		} else {

			if (Yii::app()->user->encrypting($this->email . $this->password) !== $user->password && $this->password !== $user->password) {
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			} else {
				if($user->status == 0)
				{
						$this->errorCode = self::ERROR_STATUS_NOTACTIV;
				} else {
					$this->_id = $user->id;
					$this->email = $user->email;
					$this->errorCode = self::ERROR_NONE;
				}
			}
		}

		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}
}