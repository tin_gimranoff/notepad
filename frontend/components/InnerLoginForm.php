<?php

/**
 * Class InnerLoginForm
 *
 * Widget of login form for embedding to layout.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class InnerLoginForm extends CWidget {

    public function init() {}

    public function run() {

        $loginForm = new LoginForm();

        $this->render('application.views.site._inner_login_form', array(
                'model' => $loginForm
            ));
    }
}