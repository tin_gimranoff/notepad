<?php
class UserLogin extends CWidget
{
	public function init() {}

	public function run()
	{
	    if (Yii::app()->user->isGuest) {

	        $model = new LoginForm();

	        $loginData = request()->getPost('LoginForm');
	        if (sizeof($loginData)) {
	            $model->attributes = $loginData;

	            if ($model->validate()) {
	                $this->controller->refresh();
	            }
	        }
                
                $restore_model = new UserRecoveryForm();
                $UserRecoveryFormData = request()->getPost('UserRecoveryForm');
                if(sizeof($UserRecoveryFormData))
                {
                        $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
                        $max = 8;
                        $size = strlen($chars)-1;
                        $password = null;
                        while($max--) 
                        $password .= $chars[rand(0,$size)];
                        $restore_model->attributes = $UserRecoveryFormData;
                        if($restore_model->validate())
                        {
                            if($restore_model->user_id)
                            {
                                    $user = User::model()->findByPk($restore_model->user_id);
                                    $user->password = Helper::encrypting($user->email . $password);
                                    if($user->save())
                                    {
                                            $email = Yii::app()->mailer;
                                            $email->CharSet = 'utf-8';
                                            $email->ContentType = 'text/html';
                                            $email->From = Yii::app()->params['adminEmail'];
                                            $email->FromName = 'Robot Notepad';
                                            $email->ClearAddresses();
                                            $email->AddAddress($user->email);
                                            $email->Subject = 'Новый пароль';
                                            $email->Body = "Новый пароль: ".$password;
                                            $email->Send();  
                                            $this->controller->refresh();
                                    }
                            }
                        }
                }
	        $this->render('UserLogin',array('model' => $model, 'restore_model' => $restore_model));
	    } else {
	        $this->controller->refresh();
	    }
	}
	
}