<?php
/**
 * Class LastDayPopularEntries
 *
 * Widget for display last day popular entries.
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */

class LastDayPopularEntries extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_day_popular_entries', array());
    }
}