<?php

/**
 * Class GeolocateOnMain
 *
 * Widget for block of geolocation.
 * Embedded in top menu.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class GeolocateOnMain extends CWidget {

    public function init() {}

    public function run() {
        $this->render('application.views.site._geolocation');
    }
}