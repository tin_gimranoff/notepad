<?php

/**
 * Class LastWeekPopularAuthors
 *
 * Widget for display popular authors for last week.
 * Embedded in start page.
 *
 * @author andymitrich <andymitrich@gmail.com>
 * @version 1.0
 */
class LastWeekPopularAuthors extends CWidget
{

    public function init()
    {
    }

    public function run()
    {

        $this->render('application.views.site.w_last_week_popular_authors', array());
    }
}