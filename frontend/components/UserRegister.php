<?php
class UserRegister extends CWidget
{
	public function init() {}

	public function run()
	{
        $model = new RegistrationForm();
        // ajax validator
        if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
        {
            echo ActiveForm::validate($model);
            Yii::app()->end();
        }

        if (Yii::app()->user->id) {
            $this->controller->refresh();
        } else {
            if (isset($_POST['RegistrationForm'])) {

                $model->attributes = $_POST['RegistrationForm'];

                if ($model->validate()) {

                    $model->password = Helper::encrypting($model->email . $model->password);
					$model->date_created = time();
                    if ($model->save()) {
                        $email = Yii::app()->mailer;
                        $email->CharSet = 'utf-8';
                        $email->ContentType = 'text/html';
                        $email->From = Yii::app()->params['adminEmail'];
                        $email->FromName = 'Robot Notepad';
                        $email->ClearAddresses();
                        $email->AddAddress($model->email);
                        $email->Subject = 'Активация аккаунта';
                        $email->Body = "Для активации аккаунта перейдите по ссылке:<br /> ".$_SERVER['SERVER_NAME'].'/index.php?r=site/activate&u='.$model->id.'&code='.md5($model->id.$model->email.$model->password);
                        $email->Send();
						$model->email = '';
						$model->password = '';
                        $this->controller->renderPartial('application.components.views.SuccessRegister');
                    }
                }
            }
            $this->render('UserRegister', array('model' => $model));
        }
	}
	    
	
}