$(function() {
    $.fn.autoClear = function () {
        $(this).each(function() {
            $(this).data("autoclear", $(this).attr("value"));
        });
        $(this).bind('focus', function() {
                if ($(this).attr("value") == $(this).data("autoclear")) {
                    $(this).attr("value", "").addClass('autoclear-normalcolor');
                }
        }).bind('blur', function() {
                if ($(this).attr("value") == "") {
                    $(this).attr("value", $(this).data("autoclear")).removeClass('autoclear-normalcolor');
                }
            });
        return $(this);
    }
});





function equalColumns(){
    var wrapperH = $(".wrapperGroupContentHolder").height(),
        wrapperGroupHeight = $(".wrapperGroupContentHolder .main-content").height(),
        wrapperGroupHeightLeft =  $(".wrapperGroupContentHolder .left-side").height();

    if(wrapperGroupHeight>wrapperGroupHeightLeft){
      $(".wrapperGroupContentHolder .left-side").css({
        "height": wrapperH,
        marginBottom: "-40px"
      })
    }else{
      $(".wrapperGroupContentHolder .main-content").css({
        "height": wrapperH,
        marginBottom: "-40px"
      })
    }

}
function pagerBut(){
  $(".hideSlider, .bx-pager").removeAttr("style");
  var slideH = $(".main-page-slider ul li").children("div").outerHeight(),
      imgH = $(".main-page-slider ul li").outerHeight();
  var margintop;
       if(imgH > slideH ){
           marginTop = slideH-imgH+10;
       }else{
           marginTop = 10;
       }
  $(".hideSlider").css("top", marginTop);
  $(".bx-pager").css("top", marginTop);
}


//$(window).bind('resize', resizeWindow); //Fix for IE resizeWindow

$(function(){
  var fileInput = $('input[type="file"]');
  fileInput.change(function(){
    $this = $(this);
    $(this).siblings(".fakefile").find('.filetext').val($this.val());
  });

  $(' .fakefile a').on("click", function(){
      $(this).parents(".fakefile").siblings('input[type="file"]').click();
      //fileInput.click();
  }).show();

  $(".listTime li").on("click", function(){
    if($(this).hasClass("active")){
      return false;
    }
    $(this).siblings(".active").find(".radio").removeAttr('checked');
    $(this).siblings(".active").find(".radio-holder").removeClass("active");
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
    $(this).find(".radio-holder").addClass("active");
    $(this).find(".radio").attr('checked', 'checked');
  });
});

$(function() {
if(jQuery().sortable){
 $('.sortable').sortable({
    connectWith: ".sortable",
    handle: ".handlerIco"
  })

    var $tabs = $(".tabs").tabs();

    var $tab_items = $( ".navGroupList li", $tabs ).droppable({
      tolerance: "pointer",
      accept: ".sortable li",
      hoverClass: "ui-tabs-active",
      drop: function( event, ui ) {
        var $item = $( this );
        var $list = $( $item.find( "a" ).attr( "href" ) )
          .find( ".sortable" );

        ui.draggable.hide( "slow", function() {
          //$tabs.tabs( "option", "active", $tab_items.index( $item ) );
          $( this ).appendTo( $list ).show( "slow" );
        });
      }
    });
  }
});
