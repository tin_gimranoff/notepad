$(function() {
    $(".autoclear").autoClear();

    $(".tag-list").tagit();

    $('.autocomplete').typeahead([
        {
            name: 'countries',
            local: [
                "php",
                "python"
            ]
        }
    ]);

    $("#menu_item_login").on("click", function() {
        $(".top_switchable").hide();
        $("#block_authority").show();
    });

    $("#menu_item_bloknot").on("click", function() {
        $(".top_switchable").hide();
        $("#block_bloknot_submenu").show();
    });

    $("#menu_item_geolocate").on("click", function() {
        $(".top_switchable").hide();
        $("#block_geolocation").show();
    });

    $("#menu_item_filter").on("click", function() {
        $(".top_switchable").hide();
        $("#block_filter").show();
    });

    $("#menu_item_search").on("click", function() {
        $(".top_switchable").hide();
        $("#block_search").show();
    });

    $("#menu_item_notification").on("click", function() {
        $(".top_switchable").hide();
        $("#block_notification").show();
    });
});