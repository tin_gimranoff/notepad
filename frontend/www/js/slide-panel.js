var window_height,
	panel_height,
	footer_visible;

$(function() {
    var panel = new slidePanel;
    panel.init();

    // Клик по абзацу
    $('.articleContent p').on('mouseup', function(event){
        var txt = get_selected_text();
        if (event.which === 1){
            // Если просто кликнули - выдвигаем наполовину панель, если выделили - показываем контекстное меню
            if (txt.length === 0){
                if (panel.get_status() === 'closed'){
                    panel.slide_panel('open');
                }
                $('.articleContent p').removeClass('active opacity');
                $(this).addClass('active');
                $(this).siblings('p').addClass('opacity');
            }else{
                var x = event.pageX;
                var y = event.pageY+15;
                $('.context-menu').css({
                    'left': x+'px',
                    'top': y+'px'
                });
            }
        }
    });
    // Прячем контекстное меню
    $(document).click(function(){
        var txt = get_selected_text();
        if (txt.length === 0){
            $('.context-menu').css({
                'left': '-999px',
                'top': '0px'
            });
        }
    });

    window_height = document.documentElement.clientHeight == 0 ? document.body.clientHeight : document.documentElement.clientHeight;
    panel_height = parseInt(window_height) - parseInt($('#slide-panel .wrapper').css('margin-top'));
    $('#slide-panel .wrapper').height(panel_height+'px');
    $('#slide-panel .scroller').height(panel_height+'px');

    $('#slide-panel .scroller').scroll(function(){
        panel.onScroll();
    });

	$(document).scroll(function(){
		footer_visible = document_scroll();
	});
});

function document_scroll(){
	var bottom_border = $(document).scrollTop() + window_height;
	var footer_top = $('#footer').offset().top;
	if (bottom_border > footer_top){
		var footer_visible_height = bottom_border - footer_top;
		$('#slide-panel .wrapper').height(panel_height - footer_visible_height);
		$('#slide-panel .scroller').height(panel_height - footer_visible_height);

		return true;
	}else{
		if (footer_visible === true){
			$('#slide-panel .wrapper').height(panel_height);
			$('#slide-panel .scroller').height(panel_height);
		}

		return false;
	}
}

function slidePanel(){

    var status = 'closed';
    var article_start_width = $('.article').width();
    var container_width = $('.article').parent().width();
    var article_start_margin = (container_width - article_start_width) / 2;
    var statuses = {
        'closed': {
            'class': 'closed',
            'slide_width': 50 / container_width * 100,
            'open_status': 'half',
            'px_max_width': 0.2 * container_width,
            'px_min_width': 0
        },
        'half': {
            'class': 'half-opened',
            'slide_width': 48,
            'open_status': 'opened',
            'close_status': 'closed',
            'px_max_width': 0.8 * container_width,
            'px_min_width': 0.2 * container_width
        },
        'opened': {
            'class': 'opened',
            'slide_width': 99.7,
            'close_status': 'half',
            'px_max_width': container_width,
            'px_min_width': 0.8 * container_width
        }
    };

    // Drag
    var drag_panel,
        article_start,
        panel_start,
        cursor_start;

    // Scroll
    this.top;

    var nAgt = navigator.userAgent;

    function check_position(old_status, self){
        var current_width = $('#slide-panel').width();
        if (current_width > statuses['closed']['px_min_width'] && current_width < statuses['closed']['px_max_width'] && old_status !== 'closed'){
            status = 'closed';
            $('#slide-panel').removeClass(statuses[old_status]['class']);
            $('#slide-panel').addClass(statuses[status]['class']);
        }else if (current_width > statuses['half']['px_min_width'] && current_width < statuses['half']['px_max_width'] && old_status !== 'half'){
            if (old_status === 'opened'){
                $('.article').show();
//              $('.article').css({'margin-left': article_start_margin});
//              $('.article').css({'margin-right': article_start_margin});
            }
            status = 'half';
            $('#slide-panel').removeClass(statuses[old_status]['class']);
            $('#slide-panel').addClass(statuses[status]['class']);
            if (old_status === 'closed'){
                // При появлении контента шторки вычисляем ширину скроллбара и прячем его
                scrollbar_width = $('#slide-panel .scroller').width() - $('#slide-panel .scroller-container').width();
                $('#slide-panel .scroller').css({'marginRight': '-'+scrollbar_width+'px'});

                self.onScroll();
            }
        }else if (current_width > statuses['opened']['px_min_width'] && current_width < statuses['opened']['px_max_width'] && old_status !== 'opened'){
            status = 'opened';
            $('#slide-panel').removeClass(statuses[old_status]['class']);
            $('#slide-panel').addClass(statuses[status]['class']);
//          $('.article').css({'margin-left': '-9000px'});
            $('.article').hide();
        }

		if (old_status !== status){
			document_scroll();
		}
    };

    function dragMouseDown(data){
        cursor_start = data.clientX,
        panel_start = $('#slide-panel').width();
        article_start = $('.article').width();
        drag_panel = true;

        if (nAgt.indexOf("MSIE")!=-1){
            $('div, p').attr('unselectable', 'on');
        }else{
            $('#content, #slide-panel').css({
                '-moz-user-select': '-moz-none',
                '-o-user-select': 'none',
                '-khtml-user-select': 'none',
                '-webkit-user-select': 'none',
                'user-select': 'none'
            });
            $('div, p').attr('unselectable', 'on');
        }
    };

    function dragMouseUp(self){
        drag_panel = false;

        if (nAgt.indexOf("MSIE")!=-1){
            $('#content *').attr('unselectable', 'off');
        }else{
            $('#content, #slide-panel').css({
                '-moz-user-select': 'initial',
                '-o-user-select': 'initial',
                '-khtml-user-select': 'initial',
                '-webkit-user-select': 'initial',
                'user-select': 'initial'
            });
            $('div, p').attr('unselectable', 'off');
        }

        self.get_comments_pos();
    };

    function dragMouseMove(data, self){
        if (drag_panel === true){
            var cursor_diff;

            cursor_diff = data.clientX - cursor_start;
            var new_panel = panel_start - cursor_diff;
            $('#slide-panel').width(new_panel);

            if (status !== 'opened'){
                var new_article = container_width - new_panel - article_start_margin;
                $('.article').width(new_article);
            }
            check_position(status, self);
        }
    };

    this.init = function(){
        var self = this;
        drag_panel = false;
        $('.article').css({'margin-left': article_start_margin});

        $('.open-panel').on('click', function(){
            self.slide_panel('open');

			return false;
        });
        $('.close-panel').on('click', function(){
            self.slide_panel('close');

			return false;
        });

        $('.drag-panel').on('mousedown', function(data, obj){
            dragMouseDown(data);
        });
        $(document).on('mouseup', function(){
            dragMouseUp(self);
        });
        $(document).on('mousemove', function(data, obj){
            dragMouseMove(data, self);
        });
    };

    this.slide_panel = function(action){
        var new_status,
            self = this;

        new_status = statuses[status][action+'_status'];
        var px_panel_width = statuses[new_status]['slide_width'] * container_width / 100;
        $('#slide-panel').animate({'width': px_panel_width+'px'}, {
            progress: function(){
                var current_panel = $('#slide-panel').width();
                if (status !== 'opened'){
                    var new_article = container_width - current_panel - article_start_margin;
                    $('.article').width(new_article);
                }
                check_position(status, self);
            },
            complete: function(){
                self.get_comments_pos();
            }
        });

        if (new_status === 'closed'){
            $('.articleContent p').removeClass('active opacity');
        }
    };

    this.get_status = function(){
        return status;
    };

    this.get_comments_pos =  function(){
        if (!$('.slide-panel-controls li.comments').hasClass('comments-fixed')){
            this.top = $('.when-opened li.comments').offset().top - parseInt($('#slide-panel .wrapper').css('margin-top')) + $('#slide-panel .scroller').scrollTop();
        }
    };

    this.onScroll = function(){
        var y = $('#slide-panel .scroller').scrollTop();
        if (y >= this.top && this.top > 50){
            if (!$('.slide-panel-controls li.comments').hasClass('comments-fixed')){
                $('.slide-panel-controls li.comments').addClass('comments-fixed');
                $('.slide-panel-controls').append('<li class="comments-holder"></li>');
                $('.slide-panel-controls li.comments-holder').css({'height': $('.slide-panel-controls li.comments').height()+'px'});
            }
        }else{
            if ($('.slide-panel-controls li.comments').hasClass('comments-fixed')){
                $('.slide-panel-controls li.comments').removeClass('comments-fixed');
                $('.slide-panel-controls li.comments-holder').remove();
            }
        }
    };
}

function get_selected_text(){
    var txt = '';
    if (window.getSelection){ // Not IE
        txt = window.getSelection().toString();
    }else{ // IE
        txt = document.selection.createRange().text;
    }

    return txt;
}
