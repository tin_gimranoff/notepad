	var myMap;
  var myCollection;

    ymaps.ready(function () {
        myMap = new ymaps.Map("map", {
            center: [ymaps.geolocation.latitude, ymaps.geolocation.longitude],
            zoom: 7
        });

        myCollection = new ymaps.GeoObjectCollection({}, {
           preset: 'twirl#redIcon', //все метки красные
           draggable: false // и их можно перемещать
        });
		
		myMap.controls.add('mapTools').add('typeSelector').add('smallZoomControl');
		
    myMap.events.add('click', function (e) {
      myCollection.removeAll();
      myCollection.add(new ymaps.Placemark(e.get('coordPosition')));
      myMap.geoObjects.add(myCollection);
      var region = '';
      var city = '';
      var street = '';
      var house = '';
      var str = new Array();
      var getStr = 'lat='+e.get('coordPosition')[0]+'&lng='+e.get('coordPosition')[1];
      var myGeocoder = ymaps.geocode(e.get('coordPosition'), {kind: 'locality'});
      $("#address").attr('value', '');
      myGeocoder.then(
        function (res) {
            var nearest = res.geoObjects.get(0);
            if(nearest)
            {
              if(!nearest.properties.get('metaDataProperty').GeocoderMetaData.AddressDetails.Country.AdministrativeArea)
                region = nearest.properties.get('metaDataProperty').GeocoderMetaData.AddressDetails.Country.AddressLine;
              else
                region = nearest.properties.get('metaDataProperty').GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName;
              if(region)
                getStr = getStr+'&region='+region;
              city = nearest.properties.get('name');
              if(city)
                str.push(city);
              $("#region :contains('"+region+"')").attr("selected", "selected");
              $(".select div.text").empty().append(region);
              $("li").removeClass("selected sel");
              $("li:contains('"+region+"')").addClass('selected sel');
              var myGeocoder = ymaps.geocode(e.get('coordPosition'), {kind: 'street'});
              myGeocoder.then(
                function (res) {
                    var nearest = res.geoObjects.get(0);
                    if(nearest)
                      street = nearest.properties.get('name');
                    if(street)
                      str.push(street);
                    var myGeocoder = ymaps.geocode(e.get('coordPosition'), {kind: 'house'});
                    myGeocoder.then(
                      function (res) {
                          var nearest = res.geoObjects.get(0);
                          if(nearest)
                            house = nearest.properties.get('name');
                          if(house)
                            str.push(house);
                          if(str[0])
                          {
                            $("#address").attr('value', str[0]);
                            getStr = getStr+"&city="+str[0];
                            if(str[2])
                            {
                              $("#address").attr('value', $("#address").attr('value')+', '+str[2]);
                              getStr = getStr+"&address="+str[2];
                            }
                            else
                            {
                              if(str[1])
                              {
                                $("#address").attr('value', $("#address").attr('value')+', '+str[1]);
                                getStr = getStr+"&address="+str[1];
                              }
                            }
                          }
                          $.get('/index.php?r=site/setaddress&'+getStr, function(data) {});
                      },
                      function (err) {
                          alert('Ошибка');
                      }
                    );
                },
                function (err) {
                    alert('Ошибка');
                }
              );
            } else {
              alert('К сожалению место не может быть опознано. Выберите другую точку.');
            }
        },
        function (err) {
            alert('Ошибка');
        }
    );
    });

		$("#region").change(function(){
			var myGeocoder = ymaps.geocode($("#region").val());
			myGeocoder.then(
			    function (res) {
			        myMap.panTo([res.geoObjects.get(0).geometry.getCoordinates()]);
			    },
			    function (err) {
			        alert('Ошибка');
			    }
			);
		});

	
    });
	function addPlacemark()
	{
    myCollection.removeAll();
    myCollection.add(new ymaps.Placemark(
            [ymaps.geolocation.latitude, ymaps.geolocation.longitude],
            {
                balloonContentHeader: ymaps.geolocation.country,
                balloonContent: ymaps.geolocation.city,
                balloonContentFooter: ymaps.geolocation.region
            }
        ));
		myMap.geoObjects.add(myCollection);
		myMap.panTo([ymaps.geolocation.latitude, ymaps.geolocation.longitude], {duration: 1000});
		var myGeocoder = ymaps.geocode([ymaps.geolocation.latitude, ymaps.geolocation.longitude], {kind: 'house'});
		myGeocoder.then(
		    function (res) {
		        var nearest = res.geoObjects.get(0);
				$("#address").empty().attr('value',ymaps.geolocation.city+', '+nearest.properties.get('name'));
				$.get('/index.php?r=site/setaddress&region='+ymaps.geolocation.region+'&city='+ymaps.geolocation.city+'&address='+nearest.properties.get('name')+'&lat='+ymaps.geolocation.latitude+'&lng='+ymaps.geolocation.longitude, function(data) {});
		    },
		    function (err) {
		        alert('Ошибка');
		    }
		);
		$("#region :contains('"+ymaps.geolocation.region+"')").attr("selected", "selected");
		$(".select div.text").empty().append(ymaps.geolocation.region);
		$("li").removeClass("selected sel");
		$("li:contains('"+ymaps.geolocation.region+"')").addClass('selected sel');
	}