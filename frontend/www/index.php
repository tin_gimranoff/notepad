<?php

header('Content-Type: text/html; charset=utf-8');

if($_SERVER['HTTP_HOST'] == 'notepad.local.ru')
{
	defined('YII_DEBUG') or define('YII_DEBUG',true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

	if (YII_DEBUG) {
    	ini_set('display_errors', 1);
   	 	error_reporting(E_ALL);
	}

}

define('ROOTPATH', dirname(dirname(dirname(__FILE__))));
define('FRONTPATH', ROOTPATH . DIRECTORY_SEPARATOR . 'frontend' . DIRECTORY_SEPARATOR);
define('COMMONPATH', ROOTPATH . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR);

$yii = COMMONPATH . 'lib/framework/yii.php';
$config = FRONTPATH .'config/main.php';

require_once($yii);
$app = Yii::createWebApplication($config);
require_once(COMMONPATH . 'global.php');
$app->run();